﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.IO;
using MPS.Library;

namespace MPS.DLayer
{
    public class dcommon
    {
        public static string ServerName = "";
        string SrcDatabase = "MPS";
        private SqlConnection Conn = new SqlConnection();
        public static int ConnectionTimeOut = 15;        

        public dcommon()
        {
            ServerName = GetServerName();
            if (Getconfigdata("ConnectionTimeout").Trim() != "")
            {
                ConnectionTimeOut = Convert.ToInt32(Getconfigdata("ConnectionTimeout"));
            }
        }

        public int ExecuteQuery(string sqlstr)
        {            
            DataSet ds;
            int i;
            OpenConnection();
            SqlCommand Cmd = new SqlCommand(sqlstr, Conn);
            Cmd.CommandTimeout = ConnectionTimeOut;
            i = Cmd.ExecuteNonQuery();
            Cmd.Dispose();
            CloseConnection();
            return i;
        }

        public object ExecuteQuery(string sqlstr, SqlTransaction Trans)
        {
            int i;
            SqlCommand Cmd = new SqlCommand(sqlstr, Conn, Trans);
            Cmd.CommandTimeout = ConnectionTimeOut;
            i = Cmd.ExecuteNonQuery();
            Cmd.Dispose();
            return i;

        }
        public object Execute(string sqlstr)
        {
            DataSet ds;
            try
            {
                OpenConnection();

                SqlCommand Cmd = new SqlCommand(sqlstr, Conn);
                Cmd.CommandTimeout = ConnectionTimeOut;
                ds = new DataSet();               
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(ds);
                Cmd.Dispose();
                CloseConnection();
                return ds;
                
            }
            catch (Exception Ex)
            {
                CloseConnection();
                //ErrorMessageClass.ErrorMessage = Ex.Message;
                return Ex.Message;                
            }
        }

        public object Execute(string sqlstr, SqlTransaction Trans)
        {
            DataSet ds;
            try
            {               

                SqlCommand Cmd = new SqlCommand(sqlstr, Conn, Trans);
                Cmd.CommandTimeout = ConnectionTimeOut;
                ds = new DataSet();               
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(ds);
                Cmd.Dispose();
                return ds;

            }
            catch (Exception Ex)
            {                
                //ErrorMessageClass.ErrorMessage = Ex.Message;
                return Ex.Message;
            }
        }

        public DataTable ExecuteDatatable(string Sql)
        {
            DataTable Dt = new DataTable();

            object obj = Execute(Sql);
            if (obj is DataSet)
            {
                if (((DataSet)obj).Tables.Count > 0)
                {
                    Dt = ((DataSet)obj).Tables[0];
                }
            }

            return Dt;
        }

        public DataSet ExecuteDataSet(string Sql)
        {
            DataSet Ds = new DataSet();

            object obj = Execute(Sql);
            if (obj is DataSet)
            {
                Ds = (DataSet)obj;
            }

            return Ds;
        }     

        public void OpenConnection()
        {
            if (Conn.State != ConnectionState.Open)
            {
                string Connstring = "Data Source=" + ServerName + ";Initial Catalog=" + SrcDatabase + ";User ID=ba;Password=sql2008";

                Conn.ConnectionString = Connstring;

                Conn.Open();
            }
        }

        public void CloseConnection()
        {
            if (Conn.State != ConnectionState.Closed)
            {
                Conn.Close();
            }
        }

        public int ExecuteSqlCommand(string Sql,string ParameterName,byte[] ParameterValue)
        {
            int i = 0;
            try
            {
                OpenConnection();
                string SQL = Sql;

                SqlCommand Scmd = new SqlCommand();
                Scmd.Connection = Conn;
                Scmd.CommandTimeout = ConnectionTimeOut;
                Scmd.CommandText = SQL;
                Scmd.Parameters.AddWithValue(ParameterName, ParameterValue as byte[]);

                i = Scmd.ExecuteNonQuery();
                CloseConnection();
                return i;
            }
            catch
            {
                CloseConnection();
                return 0;                
            }
        }

        public int ExecuteSqlCommand(string Sql, string ParameterName, byte[] ParameterValue, SqlTransaction Transcation)
        {
            int i = 0;
            try
            {              
                string SQL = Sql;

                SqlCommand Scmd = new SqlCommand();
                Scmd.Connection = Conn;
                Scmd.CommandTimeout = ConnectionTimeOut;
                Scmd.CommandText = SQL;
                Scmd.Transaction = Transcation;
                Scmd.Parameters.AddWithValue(ParameterName, ParameterValue as byte[]);

                i = Scmd.ExecuteNonQuery();               
                return i;
            }
            catch
            {               
                return 0;         
            }
        }

        public SqlTransaction BeginTransaction()
        {
            OpenConnection();
            return Conn.BeginTransaction();
        }

        public void CommitTransaction(SqlTransaction Trans)
        {            
            Trans.Commit();
            CloseConnection();
        }

        public void RollBackTransaction(SqlTransaction Trans)
        {
            Trans.Rollback();
            CloseConnection();
        }

        public string GetServerName()
        {
            string ServerNm = "";
            string[] TempArray= new string[5];
            string[] ConfigData = System.IO.File.ReadAllLines(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/config/config.inf");            
            var Temp = ConfigData.Where(x => x.ToUpper().Trim().Contains(CurrentSession.ProjectName.ToUpper()));
            if (Temp.Count() > 0)
            {
                TempArray = Temp.ElementAt(0).Split('#');
                ServerNm = TempArray[1].Trim();
            }

            return ServerNm;
        }

        public string Getconfigdata(string Tag)
        {
            string ReptLoc = "";
            string[] TempArray = new string[5];
            string[] ConfigData = System.IO.File.ReadAllLines(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/config/config.inf");
            var Temp = ConfigData.Where(x => x.ToUpper().Trim().Contains(Tag.ToUpper().Trim()));
            if (Temp.Count() > 0)
            {
                TempArray = Temp.ElementAt(0).Split('#');
                ReptLoc = TempArray[1].Trim();
            }

            return ReptLoc;
        }
    }
}

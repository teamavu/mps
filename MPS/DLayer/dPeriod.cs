﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MPS.DLayer;
using MPS.BLayer;
using MPS.Library;
using System.Data;
using System.Data.SqlClient;
using CustomControls;

namespace MPS.DLayer
{
    class dPeriod
    {
        dcommon dcom = new dcommon();
        bcommon bcom = new bcommon();
        public DataSet ExecuteDataSet(string strsql)
        {
            DataSet dbSet = dcom.ExecuteDataSet(strsql);
            return dbSet;
        }
        public object Update(libPeriod lib, string mode)
        {
            SqlTransaction SqlTrans = dcom.BeginTransaction();
            string Mstr = "";
            object id = null;
            try
            {
                if (mode != "Delete")
                {
                    if (lib.Period_Id == 0)
                    {
                        mode = "Add";
                    }
                    else if (lib.Period_Id > 0)
                    {
                        mode = "Edit";
                    }
                }
                else
                {
                    mode = "Delete";
                }
                Mstr = "exec [SpSet].[SetPeriod] @Period_Id="+lib.Period_Id+" ,@Period_Name='" + lib.Period_Name + "',@Period_ShortName='" + lib.Period_ShortName.ToSqlString() +
                     "',@Period_FromDate='" + Convert.ToDateTime(lib.Period_FromDate).ToString("yyyyMMdd") + "',@Period_ToDate='" + Convert.ToDateTime(lib.Period_ToDate).ToString("yyyyMMdd") + "',@Period_InActive=" + lib.Period_InActive + ",@Login_User=" + CurrentSession.UserId + ",@TransType='" + mode + "'";

                id = dcom.Execute(Mstr, SqlTrans);
                
                if (id is DataSet)
                {
                    DataSet ds = (DataSet)id;
                    if(ds.Tables.Count>0)
                    {
                        string Id = ds.Tables[0].Rows[0][0].ToString();
                        if(Id!=string.Empty)
                        {
                            lib.Period_Id = ds.Tables[0].Rows[0][0].ToSafeInteger();
                        }
                    }
                }
                if (SqlTrans != null)
                {
                    dcom.ExecuteQuery("exec [SPValidate].[GetTransValid] @Page_Tag='PERIOD',@HdrId=" + lib.Period_Id + ",@Mode='" + mode + "'", SqlTrans);
                    dcom.CommitTransaction(SqlTrans);
                }
                //dcom.CommitTransaction(SqlTrans);
                return id;
            }
            catch (Exception ex)
            {
                dcom.RollBackTransaction(SqlTrans);
                return ex.Message;
            }
        }
        public object Retrive(int Id)
        {
            string Sql = "exec [SpGet].[GetPeriod] @Period_Id=" + Id + "";
            object obj = dcom.ExecuteDataSet(Sql);
            if (obj is DataSet)
            {
                return (DataSet)obj;
            }
            else
            {
                return obj.ToString();
            }
        }
       
            
    }
}

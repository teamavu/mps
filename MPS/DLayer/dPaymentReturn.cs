﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MPS.DLayer;
using MPS.Library;
using System.Data.SqlClient;

namespace MPS.DLayer
{
    public class dPaymentReturn
    {
        dcommon dcom = new dcommon();

        public object CreateEmptyTable()
        {
            try
            {
                string Sql = "exec [SpGet].[pGetEmptyPaymentReturnTable]";
                object obj = dcom.Execute(Sql);
                return obj;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public object BindGrid(long PensionerId)
        {
            try
            {
                string Sql = "exec [SpGet].[pGetBindPaymentReturn] @Payables_PensionerId=" + PensionerId + "";
                object obj = dcom.Execute(Sql);
                return obj;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public int Update(LibPaymentReturn Lib, string Mode, out int Id, out string Msg)
        {
            SqlTransaction Transac = dcom.BeginTransaction();
            try
            {
                Id = UpdatePayables(Lib, Mode, Transac);
                if (Id == 0)
                {
                    Msg = "";
                    if (Transac != null)
                    {
                        dcom.RollBackTransaction(Transac);
                    }
                    return Lib.Payables_Id;
                }

                if (Transac != null)
                {
                    dcom.CommitTransaction(Transac);
                }

                Id = 1;
                Msg = "Record Saved Successfully";
                return Lib.Payables_Id;
            }
            catch (Exception ex)
            {
                if (Transac != null)
                {
                    dcom.RollBackTransaction(Transac);
                }

                Id = 0;
                Msg = ex.Message;
                return Lib.Payables_Id;
            }
        }

        public int UpdatePayables(LibPaymentReturn Lib, string Mode, SqlTransaction Transac)
        {
            int ReturnVal = 1;
            try
            {
                if (Lib.Payables_Id > 0)
                {
                    Mode = "Edit";
                }

                string Sql = "exec SpSet.SetPaymentReturn @Payables_Id="+Lib.Payables_Id+ ",@Payables_PensionerId="+Lib.Payables_PensionerId+ ",@Payables_Reason='"+Lib.Payables_Reason.ToSqlString()
                    + "',@Payables_ReturnOnDate='"+Lib.Payables_ReturnOnDate.ToSqlString()+ "',@Payables_ReturnComment='"+Lib.Payables_ReturnComment.ToSqlString()
                    + "',@Payables_ExpensesDeduct="+Lib.Payables_ExpensesDeduct.ToSafeDouble()+ ",@LoginUser="+CurrentSession.UserId+ ",@TransType='"+Mode.ToSqlString()+"'";
                object obj = dcom.Execute(Sql, Transac);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    ReturnVal = 1;
                }
                else
                {
                    ReturnVal = 0;
                    throw new Exception(obj.ToString());
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ReturnVal;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using MPS.Library;

namespace MPS.DLayer
{
    public class dPensionerMaster
    {
        dcommon dcom = new dcommon();
        public object Update(LibPensionerMaster lib, string Mode)
        {
            SqlTransaction Trans = dcom.BeginTransaction();
            object obj = null;
            try
            {
                string str = "exec [SpSet].[SetPensionerMaster] @PMast_Id=" + lib.PMast_Id + ",@PMast_Name='" + lib.PMast_Name + "',@PMast_Address1='" + lib.PMast_Address1.ToSqlString() + "',@PMast_Address2='" + lib.PMast_Address2.ToSqlString() + "',@PMast_Address3='" + lib.PMast_Address3.ToSqlString() + "',@PMast_Address4='" + lib.PMast_Address4.ToSqlString()
                    + "',@PMast_PinNo='" + lib.PMast_PinNo.ToSqlString() + "',@PMast_RefNo='" + lib.PMast_RefNo.ToSqlString() + "',@PMast_Type=" + lib.PMast_Type
                    + ",@PMast_MonthlyAmt=" + lib.PMast_MonthlyAmt + ",@PMast_MOC=" + lib.PMast_MOC + ",@PMast_PaymentMethod=" + lib.PMast_PaymentMethod
                    + ",@PMast_AdharNo='" + lib.PMast_AdharNo.ToSqlString() + "',@PMast_VoterIdNo='" + lib.PMast_VoterIdNo.ToSqlString()
                    + "',@PMast_PanNo='" + lib.PMast_PanNo.ToSqlString() + "',@PMast_DriveLicNo='" + lib.PMast_DriveLicNo.ToSqlString()
                    + "',@PMast_MobileNo='" + lib.PMast_MobileNo.ToSqlString() + "',@PMast_OtherRefNo='" + lib.PMast_OtherRefNo.ToSqlString()
                    + "',@PMast_BankACNo='" + lib.PMast_BankACNo.ToSqlString() + "',@PMast_BankBranch='" + lib.PMast_BankBranch.ToSqlString()
                    + "',@PMast_BankIFSCCode='" + lib.PMast_BankIFSCCode.ToSqlString() + "',@PMast_Remark='" + lib.PMast_Remark.ToSqlString()
                    + "',@PMast_RenewOnMonth='" + lib.PMast_RenewOnMonth.ToSqlString() + "',@PMast_LastRenewOn='" + lib.PMast_LastRenewOn.ToSqlString()
                    + "',@PMast_InActive=" + lib.PMast_InActive + ",@PMast_InActiveOn='" + lib.PMast_InActiveOn.ToSqlString()
                    + "',@PMast_InActiveReason='" + lib.PMast_InActiveReason.ToSqlString()
                    + "',@PMast_MembershipNo='" + lib.PMast_MembershipNo.ToSqlString() + "',@PMast_Bank='" +lib.PMast_BankName.ToSqlString() +"',@Loginuser=" + CurrentSession.UserId+ ",@TransType='"+Mode.ToSqlString()+"'";
                 obj = dcom.Execute(str, Trans);
               
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if(ds.Tables[0].Rows.Count > 0)
                    {
                        if(ds.Tables[0].Rows[0]["Column1"].ToString() !=string.Empty)
                        {
                            lib.PMast_Id = ds.Tables[0].Rows[0]["Column1"].ToSafeInteger();
                        }
                    }
                
                }

                else { throw new Exception(obj.ToString()); }
                dcom.ExecuteQuery("exec [SPValidate].[GetTransValid] @Page_Tag='PENSIONERMASTER',@HdrId=" + lib.PMast_Id + ",@Mode='" + Mode + "'", Trans);
                dcom.CommitTransaction(Trans);
            }
            catch (Exception ex)
            {
                if (Trans != null)
                {
                    dcom.RollBackTransaction(Trans);
                }
                throw new Exception(ex.Message);
            }
            return obj;
        }

        public object Retrieve(int id)
        {
            //SqlTransaction Trans = dcom.BeginTransaction();
            object obj = null;
            try
            {
                string str = "exec SpGet.RetrievePensionerMaster @Pmast_id="+id+"";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                { }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return obj;
        }

        public DataTable CheckDuplicateAdharNo(int id,string AdharNo)
        {
            object obj = null;
            DataTable dt = null;
            try
            {
                string str = "exec SpGet.DuplicateAdharNo @Pmast_id=" + id + ",@Pmast_AdharNo='"+AdharNo.ToSqlString()+"'";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    dt = ds.Tables[0];
                }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        } 

        public DataTable DuplicatePanNo(int id,string PanNo)
        {
            object obj = null;
            DataTable dt = null;
            try
            {
                string str = "exec SpGet.DuplicatePanNo @Pmast_id=" + id + ",@Pmast_PanNo='" + PanNo.ToSqlString() + "'";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    dt = ds.Tables[0];
                }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        }

        public DataTable DuplicateVoterId(int id,string VoterId)
        {
            object obj = null;
            DataTable dt = null;
            try
            {
                string str = "exec SpGet.DuplicateVoterId @Pmast_id=" + id + ",@Pmast_VoterId='" + VoterId.ToSqlString() + "'";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    dt = ds.Tables[0];
                }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        }

        public DataTable DuplicateDriverLicNo(int id,string DriverLicNo)
        {
            object obj = null;
            DataTable dt = null;
            try
            {
                string str = "exec SpGet.DuplicateDriveLicNo @Pmast_id=" + id + ",@Pmast_DriveLicNo='" + DriverLicNo.ToSqlString() + "'";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    dt = ds.Tables[0];
                }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        }

        public DataTable DuplicateMembershipNo(int id, string MemberShipNo)
        {
            object obj = null;
            DataTable dt = null;
            try
            {
                string str = "exec SpGet.DuplicateMembershipNo @Pmast_Id=" + id + ",@Pmast_MembershipNo='" + MemberShipNo.ToSqlString() + "'";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    dt = ds.Tables[0];
                }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        }

        public DataTable DuplicateRefNo(int id,int Type,string RefNo)
        {
            object obj = null;
            DataTable dt = null;
            try
            {
                string str = "exec SpGet.DuplicateRefNoOnType @Pmast_Id=" + id + ",@Pmast_RefNo='" + RefNo.ToSqlString() + "',@Pmast_Type="+Type+"";
                obj = dcom.Execute(str);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    dt = ds.Tables[0];
                }
                else { throw new Exception(obj.ToString()); }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return dt;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MPS.Library;
using MPS.BLayer;

namespace MPS.DLayer
{
    public class dPayables
    {
        dcommon dcom = new dcommon();

        public object CreateEmptyTable()
        {
            try
            {
                string Sql = "exec [SpGet].[pGetCreateEmptyPayablesTable]";
                object obj = dcom.Execute(Sql);
                return obj;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public object BindGrid(int PeriodId, int TypeId)
        {
            try
            {
                string Sql = "exec [SpGet].[pGetBindPayableGrid] @Period_Id=" + PeriodId + ",@Type_Id=" + TypeId + "";
                object obj = dcom.Execute(Sql);
                return obj;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public int Update(LibPayables Lib, string Mode, out int Id, out string Msg)
        {
            SqlTransaction Transac = dcom.BeginTransaction();
            try
            {
                Id = UpdatePayables(Lib, Mode, Transac);
                if (Id == 0)
                {
                    Msg = "";
                    if (Transac != null)
                    {
                        dcom.RollBackTransaction(Transac);
                    }
                    return Lib.Payables_Id;
                }

                if (Transac != null)
                {
                    dcom.CommitTransaction(Transac);
                }

                Id = 1;
                Msg = "Record Saved Successfully";
                return Lib.Payables_Id;
            }
            catch (Exception ex)
            {
                if (Transac != null)
                {
                    dcom.RollBackTransaction(Transac);
                }

                Id = 0;
                Msg = ex.Message;
                return Lib.Payables_Id;
            }
        }

        public int UpdatePayables(LibPayables Lib, string Mode, SqlTransaction Transac)
        {
            int ReturnVal = 1;
            try
            {
                if (Lib.PayablesDetails != null)
                {
                    foreach (DataRow Dr in Lib.PayablesDetails.Rows)
                    {
                        if (Dr["Payables_Deleted"].ToSafeInteger() == 1)
                        {
                            Mode = "Delete";
                        }
                        else if (Dr["Payables_Id"].ToSafeInteger() == 0)
                        {
                            Mode = "Add";
                            Dr["Payables_Id"] = 0;
                        }
                        else if (Dr["Payables_Id"].ToSafeInteger() > 0)
                        {
                            Mode = "Edit";
                        }

                        string Sql = "exec [SpSet].[pSetPayables] @Payables_Id=" + Dr["Payables_Id"] + ",@Payables_PensionerId=" + Dr["Payables_PensionerId"] +
                            ",@Payables_PeriodId=" + Lib.Payables_PeriodId + ",@Payables_TypeId=" + Lib.Payables_TypeId + ",@Payables_Rate=" + Dr["Payables_Rate"] +
                            ",@Payables_Amt=" + Dr["Payables_Amt"] + ",@Payables_MOC=" + (Dr["Payables_MOC"].ToString()==string.Empty ? "0.00" : Dr["Payables_MOC"])+ ",@Payables_BankAccNo='" + Dr["Payables_BankAccNo"] +
                            "',@Payables_BankIFSCCode='" + Dr["Payables_BankIFSCCode"] + "',@LoginUser='" + CurrentSession.UserId + "',@TransType='" + Mode + "'";
                        object obj = dcom.Execute(Sql, Transac);
                        if (obj is DataSet)
                        {
                            DataSet ds = (DataSet)obj;
                            ReturnVal = 1;
                        }
                        else
                        {
                            ReturnVal = 0;
                            throw new Exception(obj.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ReturnVal;
        }
    }
}

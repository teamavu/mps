﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MPS.DLayer;
using MPS.BLayer;
using MPS.Library;
using System.Data;
using System.Data.SqlClient;
using CustomControls;

namespace MPS
{
    /// <summary>
    /// Interaction logic for PeriodMaster.xaml
    /// </summary>
    public partial class PeriodMaster : Page
    {

        public PeriodMaster()
        {

            InitializeComponent();
        }
        bcommon bcom = new bcommon();
        dcommon dcom = new dcommon();
        //ModeClass ModeC = new ModeClass();
        bool isFirstLoad = true;
        bPeriod bper = new bPeriod();
        libPeriod lper = new libPeriod();
        static string Mode ;
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (isFirstLoad)
            {
                isFirstLoad = false;
                UIServices.SetBusyState();
                Mode = "Load";
                SetPageMode();
            }
        }
        public void ClearPageControls()
        {
            txtid.Text = "";
            txtPeriodName.Text = "";
            txtshortcutPeriodName.Text = "";
            txtmessage.Text = "";
            txttodate.Text = "";
            txtfromdate.Text = "";
            ckActive.IsChecked = false;

        }
        public void EnableDisableButtons()
        {
            if (Mode == "Find")
            {
                BtnFind.IsEnabled = true;
                BtnAddNew.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnSave.IsEnabled = false;
                BtnEdit.IsEnabled = true;
                BtnRefresh.IsEnabled = false;
                BtnDelete.IsEnabled = true;
            }
            else if (Mode == "Load")
            {
                BtnFind.IsEnabled = true;
                BtnAddNew.IsEnabled = true;
                BtnRefresh.IsEnabled = false;
                BtnCancel.IsEnabled = false;
                BtnSave.IsEnabled = false;
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
            }
            else if (Mode == "Add")
            {
                BtnRefresh.IsEnabled = true;
                BtnSave.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnAddNew.IsEnabled = false;
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
                BtnFind.IsEnabled = false;
            }
            else if (Mode == "Edit")
            {
                BtnRefresh.IsEnabled = true;
                BtnSave.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnAddNew.IsEnabled = false;
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
                BtnFind.IsEnabled = false;
            }

        }
        public void SetPageMode()
        {
            MainGrid.Focus();
            
            if (Mode == "Load")
            {
                txtmessage.Text = "";

                ControlGrid.IsEnabled = false;
                ClearPageControls();
                EnableDisableButtons();

            }
            else if (Mode == "Add")
            {
                txtmessage.Text = "";
                ControlGrid.IsEnabled = true;
                ClearPageControls();
                EnableDisableButtons();

                txtPeriodName.Focus();
            }
            else if (Mode == "Find")
            {
                txtmessage.Text = "";
                EnableDisableButtons();
                ControlGrid.IsEnabled = false;

            }
            else if (Mode == "Edit")
            {
                txtmessage.Text = "";
                EnableDisableButtons();
                ControlGrid.IsEnabled = true;
            }
        }
        public bool validateToSave()
        {
            string Msg = string.Empty;
            if (txtPeriodName.Text.Trim() == "")
            {
                Msg = Msg + "Enter Period Name!!\r\n";
            }
            if (txtfromdate.Text.Trim() == "")
            {
                Msg = Msg + "Select From Date!!\r\n";
            }
            if (txttodate.Text.Trim() == "")
            {
                Msg = Msg + "Select To Date!!\r\n";
            }
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "")
            {
                if (ERBFunctions.CompareDbDate(ERBFunctions.ConvertToDate(txtfromdate.Text), ERBFunctions.ConvertToDate(txttodate.Text), "greaterthan") == false)
                {
                    Msg = "To Date must be Greater than From Date";
                    txttodate.Focus();
                }
                else
                {
                    //Convert.ToDateTime(txtfromdate.Text).Month + 3 == Convert.ToDateTime(txttodate.Text).Month
                    //DateTime date1;
                    //DateTime date2;
                    //date1 = Convert.ToDateTime(txtfromdate.Text);
                    //date2 = Convert.ToDateTime(txttodate.Text);

                    //int dif = Math.Abs((date2.Month - date1.Month) + 12 * (date2.Year - date1.Year));
                    //if (dif == 3)
                    //{
                    //}
                    //else
                    //{
                    //    Msg = "Date Difference Should be of 3 Months";
                    //}
                }
            }
            if (Msg != "")
            {
                MessageBox.Show(Msg, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                
                if (txtPeriodName.Text.Trim() == "")
                {
                    txtPeriodName.Focus();
                }
                else if (txtfromdate.Text.Trim() == "")
                {
                    txtfromdate.Focus();
                }

                else if (txttodate.Text.Trim() == "")
                {
                    txttodate.Focus();
                }
                return false;
            }
            else
            {
                return true;
            }

        }
        
        private void BtnPanel_OnAddClick(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UIServices.SetBusyState();
            Mode = "Add";
            SetPageMode();
            txtid.Text = "0";

        }

        private void BtnPanel_OnSaveClick(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UIServices.SetBusyState();
            Object obj = new object();
            try
            {
                if (validateToSave() == false)
                {
                    return;
                }
                else
                {
                    if (bcom.MultiDuplicateCheck("select * from BaseV.PeriodMaster where Period_Deleted=0  and Period_Name ='" + txtPeriodName.Text.Trim() + "' and Period_Id !=" + txtid.Text) != true)
                    {
                        MessageBox.Show("Duplicate Period Name Found!", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                        txtPeriodName.Focus();
                        return;
                    }
                    if (bcom.MultiDuplicateCheck("select * from BaseV.PeriodMaster where Period_Deleted=0  and Period_FromDate = '" + Convert.ToDateTime(txtfromdate.Text.Trim()).ToString("yyyyMMdd") + "' and Period_ToDate = '" + Convert.ToDateTime(txttodate.Text.Trim()).ToString("yyyyMMdd") + "' and Period_Id !=" + txtid.Text) != true)
                    {
                        MessageBox.Show("Already Period Exists for the Selected Date Interval !", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                   
                    if (bcom.MultiDuplicateCheck("SELECT* FROM BaseV.PeriodMaster WHERE '" + Convert.ToDateTime(txtfromdate.Text.Trim()).ToString("yyyyMMdd") + "'  between Period_FromDate and Period_ToDate and Period_Deleted = 0 and Period_Id !=" + txtid.Text) != true)
                    {
                        MessageBox.Show("Already Period Exists for the Selected From Date Interval !", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (bcom.MultiDuplicateCheck("SELECT* FROM BaseV.PeriodMaster WHERE '" + Convert.ToDateTime(txttodate.Text.Trim()).ToString("yyyyMMdd") + "'  between Period_FromDate and Period_ToDate and Period_Deleted = 0 and Period_Id !=" + txtid.Text) != true)

                    {
                        MessageBox.Show("Already Period Exists for the Selected To Date Interval !", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    if (bcom.MultiDuplicateCheck("SELECT* FROM BaseV.PeriodMaster WHERE '" + Convert.ToDateTime(txtfromdate.Text.Trim()).ToString("yyyyMMdd") + "' <  Period_FromDate and '" + Convert.ToDateTime(txttodate.Text.Trim()).ToString("yyyyMMdd") + "' > Period_ToDate and Period_Deleted = 0 and Period_Id !=" + txtid.Text) != true)

                    {
                        MessageBox.Show("Already Period Exists Between the Selected Date Interval !", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }


                    //string GrpName = bcom.GetCommonMasterLangTag(ddlGroupName.SelectedValue);

                    FillLib();
                    obj = bper.Update(lper,Mode);
                  
                    if (obj is DataSet)
                    {
                        Mode = "Find";
                        
                        SetPageMode();
                        ClearPageControls();
                        Retrive(lper.Period_Id);
                        txtmessage.Text = "Record Saved Successfully";
                    }
                    else
                    {
                        MessageBox.Show(obj.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                }

            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        

        private void BtnPanel_OnCancelClick(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UIServices.SetBusyState();
            Mode = "Load";
            SetPageMode();
        }

        private void BtnPanel_OnDeleteClick(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UIServices.SetBusyState();
            Object obj = new object();
            try
            {
                Mode = "Delete";
                FillLib();
                obj = bper.Update(lper, Mode);
                
                if (obj is DataSet)
                {
                    Mode = "Load";
                    SetPageMode();
                    ClearPageControls();

                    txtmessage.Text = "Record Deleted Successfully";
                }
                else
                {
                    MessageBox.Show(obj.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
        public void FillLib()
        {
            lper.Period_Id = txtid.Text.ToSafeInteger();
            lper.Period_Name = txtPeriodName.Text.Trim();
            lper.Period_ShortName = txtshortcutPeriodName.Text.Trim();
            lper.Period_FromDate = txtfromdate.Text.Trim();
            lper.Period_ToDate = txttodate.Text.Trim();
           
            if (ckActive.IsChecked == true)
                lper.Period_InActive = 1;
            else
                lper.Period_InActive = 0;

        }

        private void BtnPanel_OnEditClick(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UIServices.SetBusyState();
            Mode = "Edit";

            SetPageMode();
        }

        private void BtnPanel_OnFindClick(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UIServices.SetBusyState();
            Find.FillFind("Periodmaster");
            FindPopup.IsOpen = true;

        }
        public void Retrive(int value)
        {
            object obj = bper.Retrieve(value);
            if (obj is DataSet)
            {
                DataSet ds = (DataSet)obj;
                txtid.Text = ds.Tables[0].Rows[0]["Period_Id"].ToString();
                txtPeriodName.Text = ds.Tables[0].Rows[0]["Period_Name"].ToString();
                txtshortcutPeriodName.Text = ds.Tables[0].Rows[0]["Period_ShortName"].ToSafeString();
                txtfromdate.Text = ds.Tables[0].Rows[0]["Period_FromDate"].ToSafeString();
                txttodate.Text = ds.Tables[0].Rows[0]["Period_ToDate"].ToSafeString();
               
                if (ds.Tables[0].Rows[0]["Period_InActive"].ToSafeInteger() == 1)
                    ckActive.IsChecked = true;
                else
                    ckActive.IsChecked = false;
                Mode = "Find";
                SetPageMode();
            }
        }

        private void Find_OnSelectionChanged(object sender, EventArgs e)
        {
            UIServices.SetBusyState();
            try
            {
                FindPopup.IsOpen = false;
                DataRowView Drv = Find.SelectedItem;
                txtid.Text = Convert.ToString(Drv.Row["ID"]);
                Mode = "Find";
                Retrive(Convert.ToInt32(txtid.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void BtnRefresh_Click(object sender, RoutedEventArgs e)
        {

        }

        private void txtfromdate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (txtfromdate.Text.Trim() != "")
            //{
            //    DateTime date1 = Convert.ToDateTime(txtfromdate.Text).AddMonths(3);
            //    txttodate.SelectedDate = date1;
            //}

        }
    }
}

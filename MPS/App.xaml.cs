﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.IO;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Input;

namespace MPS
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError:{0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",

          e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
          e.Exception.InnerException.Message : null));

            if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            {
                if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }

            e.Handled = true;

        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            CloseAllOpenWindows();

            DirectoryInfo Dirinf = new DirectoryInfo(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/Temp/");
            foreach (FileInfo Fileinf in Dirinf.GetFiles())
            {
                try
                {
                    Fileinf.Delete();
                }
                catch (Exception Ex)
                {

                }
            }
        }

        private void CloseAllOpenWindows()
        {
            foreach (Window Win in App.Current.Windows)
            {
                Win.Close();
            }
        }

        #region TextBoxStyleHandlers

        // TextBox
        private void txt_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox Txt = (TextBox)sender;
            Txt.SelectAll();
        }

        private void txt_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBox Txt = (TextBox)sender;
            if (!Txt.IsKeyboardFocusWithin)
            {
                Txt.Focus();
                e.Handled = true;
            }
        }
        
        // PasswordBox
        private void Pass_GotFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox Txt = (PasswordBox)sender;
            Txt.SelectAll();
        }

        private void Pass_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            PasswordBox Txt = (PasswordBox)sender;
            if (!Txt.IsKeyboardFocusWithin)
            {
                Txt.Focus();
                e.Handled = true;
            }
        }
        #endregion
    }
}

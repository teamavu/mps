﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MPS.BLayer;
using MPS.DLayer;
using MPS.Library;

namespace MPS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        dcommon dcom = new dcommon();
        bcommon bcom = new bcommon();

        public MainWindow()
        {
            InitializeComponent();
            txtusername.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CurrentSession.ResetSession();
            InformationGridGrid.Visibility = Visibility.Hidden;
            txtmessage.Text = "";
            LoadDDl();
            ExecuteMultiLogin();
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void ExecuteMultiLogin()
        {
            string Evar = Environment.GetEnvironmentVariable("#User#");
            if (Evar == null)
            {
                UserLoginGrid.Visibility = Visibility.Visible;
                txtusername.Focus();
            }
            else
            {
                PerformLogin();
            }
        }

        public void LoadDDl()
        {
            bcom.FillLOVDDL(ddlPeriod, "faFinancialYear", "", "", "");
        }

        public void GetLatestPeriod()
        {
            string str = "exec SpGet.GetLatestPeriod";
            object obj = dcom.Execute(str);
            if (obj is DataSet)
            {
                DataSet ds = (DataSet)obj;
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int Fy_id = ds.Tables[0].Rows[0][0].ToSafeInteger();
                        ddlPeriod.SelectedValue = Fy_id.ToString();
                        CurrentSession.PeriodFrom = Convert.ToDateTime(ds.Tables[0].Rows[0]["cd_PeriodFrom"]);
                        CurrentSession.PeriodTo = Convert.ToDateTime(ds.Tables[0].Rows[0]["cd_PeriodTo"]);
                    }
                }

            }

        }

        private void btnUserLoginAccept_Click(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";

            object obj = dcom.Execute("select usr_ID , usr_Login, CONVERT(nvarchar(max),usr_Password) as usr_Password from SETUP.UserMaster where usr_Deleted=0");
            if (obj is DataSet)
            {
                DataTable Dt = ((DataSet)obj).Tables[0];
                if (Dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in Dt.Rows)
                    {
                        string Password = dr["usr_Password"].ToString();
                        Password = Password.TrimEnd('\0');

                        if (Password == txtpassword.Password && dr["usr_Login"].ToString().ToUpper() == txtusername.Text.ToUpper())
                        {
                            CurrentSession.UserId = Convert.ToInt32(dr["usr_ID"]);
                            CurrentSession.UserName = dr["usr_Login"].ToString();

                           // LoadUserOrganizations();

                            UserLoginGrid.Visibility = Visibility.Hidden;
                            //InformationGridGrid.Visibility = Visibility.Visible;
                            ddlorganization.Focus();
                            ddlPeriod.SelectedIndex = 0;
                            txtmessage.Text = "";
                            GetLatestPeriod();
                            MasterWindow Mast = new MasterWindow();
                            this.Close();
                            Mast.Show();
                            return;
                        }
                        else
                        {
                            txtmessage.Text = "Incorrect Username and Password";
                        }
                    }
                }
                else
                {
                    txtmessage.Text = "Incorrect Username and Password";
                    return;
                }
            }
            else
            {
                txtmessage.Text = obj.ToString();
                return;
            }
        }

        private void btnUserLoginBack_Click(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            this.Close();
        }

        private void PerformLogin()
        {
            CurrentSession.UserId = Convert.ToInt32(Environment.GetEnvironmentVariable("#User#"));
            CurrentSession.OrgID = Convert.ToInt32(Environment.GetEnvironmentVariable("#Org#"));
            CurrentSession.OrgName = Environment.GetEnvironmentVariable("#OrgName#");
            ddlorganization.SelectedValue = CurrentSession.OrgID;
            ddlPeriod.SelectedValue = Environment.GetEnvironmentVariable("#PeriodID#");
            btnInformationGridGridAccept_Click(null, null);
        }

        private void btnInformationGridGridAccept_Click(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            GetGeneralDetails();
            if (CurrentSession.CurrencyId == 0)
            {
                txtmessage.Text = "Organization does not have a Default Currency. Please contact the Administrator.";
                return;
            }
            if (CurrentSession.BgId == 0)
            {
                txtmessage.Text = "Organization does not have a Business Group. Please contact the Administrator.";
                return;
            }
            if (CurrentSession.RoundOffLedgerID == 0)
            {
                txtmessage.Text = "A RoundOff Ledger has not been created. Please contact the Administrator.";
                return;
            }
            if (CurrentSession.OrgID == 0)
            {
                CurrentSession.OrgID = ddlorganization.SelectedValue.ToSafeInteger();
                CurrentSession.OrgName = ddlorganization.Text;
            }

            CurrentSession.AccOrgID = Convert.ToInt32(ddlorganization.SelectedValue);
            CurrentSession.PeriodID = ddlPeriod.SelectedValue.ToSafeInteger();

            MasterWindow Mast = new MasterWindow();
            this.Close();
            Mast.Show();
        }

        private void btnInformationGridGridnBack_Click(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            UserLoginGrid.Visibility = Visibility.Visible;
            InformationGridGrid.Visibility = Visibility.Hidden;
            txtusername.Focus();
        }

        private void GetGeneralDetails()
        {
            DataSet Ds = dcom.ExecuteDataSet("[SPGet].[GetGeneralDetails] @FYId=" + ddlPeriod.SelectedValue + ", @OrgId=" + ddlorganization.SelectedValue);

            #region Period
            DataTable Period = Ds.Tables[0];
            if (Period.Rows.Count > 0)
            {
                CurrentSession.PeriodFrom = Convert.ToDateTime(Period.Rows[0]["cd_PeriodFrom"]);
                CurrentSession.PeriodTo = Convert.ToDateTime(Period.Rows[0]["cd_PeriodTo"]);
            }

            DataTable PeriodClose = Ds.Tables[6];
            if (PeriodClose.Rows.Count > 0)
            {
                if (PeriodClose.Rows[0]["coms_Status"].ToString().ToUpper().Trim() == "OPEN")
                {
                    CurrentSession.IsPeriodClosed = false;
                }
                else if (PeriodClose.Rows[0]["coms_Status"].ToString().ToUpper().Trim() == "CLOSE")
                {
                    CurrentSession.IsPeriodClosed = true;
                }
            }
            #endregion

            #region Currency
            DataTable Currency = Ds.Tables[2];
            if (Currency.Rows.Count > 0)
            {
                CurrentSession.CurrencyId = Currency.Rows[0]["Currency_Id"].ToSafeInteger();
            }
            else
            {
                CurrentSession.CurrencyId = 0;
            }
            #endregion

            #region Organization
            DataTable Organization = Ds.Tables[3];
            if (Organization.Rows.Count > 0)
            {
                CurrentSession.BgId = Organization.Rows[0]["Org_BgID"].ToSafeInteger();
                if (Organization.Rows[0]["Org_ParentOrgId"].ToSafeInteger() > 0)
                {
                    CurrentSession.OrgID = Organization.Rows[0]["Org_ParentOrgId"].ToSafeInteger();
                }
            }
            else
            {
                CurrentSession.BgId = 0;
                CurrentSession.OrgID = 0;
            }
            #endregion

            #region Transaction Type Table
            CurrentSession.TransTypeTable = Ds.Tables[4];
            #endregion

            #region RoundOff Ledger
            if (Ds.Tables[5].Rows.Count > 0)
            {
                CurrentSession.RoundOffLedgerID = Ds.Tables[5].Rows[0]["al_Id"].ToSafeInteger();
                CurrentSession.RoundOffLedgerName = Ds.Tables[5].Rows[0]["al_Name"].ToString();
            }
            #endregion

            if (Ds.Tables[7].Rows.Count > 0)
            {
                CurrentSession.fapar_IsAutoChequeNo = Ds.Tables[7].Rows[0]["fapar_IsAutoChequeNo"].ToSafeInteger();
            }

        }

        public void LoadUserOrganizations()
        {
            DataTable Dt = dcom.ExecuteDatatable("[SpGet].[faGetUserOrgList] @Usr_Id=" + CurrentSession.UserId);
            CurrentSession.UserOrgList = Dt.Copy();

            ddlorganization.SelectedValuePath = "ID";
            ddlorganization.DisplayMemberPath = "Name";
            DataRow dr = Dt.NewRow();
            dr["ID"] = 0;
            dr["Name"] = "--select--";
            Dt.Rows.InsertAt(dr, 0);
            ddlorganization.ItemsSource = Dt.DefaultView;
            if (Dt.Rows.Count > 0)
            {
                int Orgid = Dt.Rows[1][1].ToSafeInteger();
                ddlorganization.SelectedValue = Orgid;
            }
            else
            {
                ddlorganization.SelectedValue = 0;
            }

        }
    }
}

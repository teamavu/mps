﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using MPS.BLayer;
using MPS.DLayer;
using MPS.Library;
using MPS.UserControls;


namespace MPS
{
    /// <summary>
    /// Interaction logic for Payables.xaml
    /// </summary>
    public partial class Payables : Page
    {
        bcommon bcom = new bcommon();
        dcommon dcom = new dcommon();
        bPayables bClass = new bPayables();
        dPayables dClass = new dPayables();
        LibPayables Lib = new LibPayables();
        ModeClass ModeC = new ModeClass();
        bool IsFirstLoad = true;

        public Payables()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if(IsFirstLoad)
            {
                UIServices.SetBusyState();
                ModeC.Mode = "Load";
                ClearPageControls();
                LoadDDL();
                CreateEmptyTable();
                EnableDisableButtons();
            }
        }

        public void ClearPageControls()
        {
            txtid.Text = "0";
            txtmessage.Text = string.Empty;
            ddlPeriod.SelectedValue = 0;
            ddlType.SelectedValue = 0;
            DGridPayablesDtls.DataContext = null;
        }

        public void LoadDDL()
        {
            if (ddlPeriod.SelectedValue == 0) { bcom.FillCustomControl(ddlPeriod, "PayablesPeriod", "", "", ""); }
            if (ddlType.SelectedValue == 0) { bcom.FillCustomControl(ddlType, "PayablesType", "", "", ""); }
        }

        public void CreateEmptyTable()
        {
            try
            {
                object obj = bClass.CreateEmptyTable();
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if (ds.Tables.Count > 0)
                    {
                        Lib.PayablesDetails = ds.Tables[0];
                        DGridPayablesDtls.DataContext = Lib.PayablesDetails.DefaultView;
                        Lib.PayablesDetails.DefaultView.RowFilter = "Payables_Deleted=0";
                    }
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }
            }
            catch(Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
        }

        public void EnableDisableButtons()
        {
            if (ModeC.Mode == "Load")
            {
                BtnSave.IsEnabled = false;
                BtnCancel.IsEnabled = true;
                BtnExport.IsEnabled = true;
            }
            else if (ModeC.Mode == "Add")
            {
                BtnSave.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnExport.IsEnabled = true;
            }
        }

        public void BindGrid()
        {
            try
            {
                object obj = bClass.BindGrid(ddlPeriod.SelectedValue.ToSafeInteger(), ddlType.SelectedValue.ToSafeInteger());
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if (ds.Tables.Count > 0)
                    {
                        Lib.PayablesDetails = ds.Tables[0];
                        DGridPayablesDtls.DataContext = Lib.PayablesDetails.DefaultView;
                        Lib.PayablesDetails.DefaultView.RowFilter = "Payables_Deleted=0";
                    }
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnFilter_Click(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            try
            {
                if (ValidateToFilter() == false)
                {
                    return;
                }

                BindGrid();
                ModeC.Mode = "Add";
                EnableDisableButtons();
            }
            catch(Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        public bool ValidateToFilter()
        {
            string Msg = string.Empty; bool Valid = true;
            if (ddlPeriod.SelectedValue == 0)
            {
                Msg = Msg + "-Please Select Period!!\r\n";
            }

            if (ddlType.SelectedValue == 0)
            {
                Msg = Msg + "-Please Select Type!!\r\n";
            }

            if (Msg != string.Empty)
            {
                MessageBox.Show(Msg, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                if (Msg.Contains("Period"))
                {
                    ddlPeriod.Focus();
                }
                else if (Msg.Contains("Type"))
                {
                    ddlType.Focus();
                }
                Valid = false;
            }
            return Valid;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int Id = 0;string Msg = string.Empty;

                if (ValidateToSave() == false)
                {
                    return;
                }

                Lib.Payables_PeriodId = ddlPeriod.SelectedValue.ToSafeInteger();
                Lib.Payables_TypeId = ddlType.SelectedValue.ToSafeInteger();

                txtid.Text = bClass.Update(Lib, ModeC.Mode, out Id, out Msg).ToString();
                if (Id == 0)
                {
                    txtmessage.Text = Msg;
                    return;
                }

                txtmessage.Text = Msg;
                ModeC.Mode = "Load";
                BindGrid();
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        public bool ValidateToSave()
        {
            string Msg = string.Empty; bool Valid = true;

            if (Lib.PayablesDetails == null)
            {
                Msg = Msg = "-Please Fill Atleast One Payables Details!!\r\n";
            }

            if (Lib.PayablesDetails != null)
            {
                if (Lib.PayablesDetails.Rows.Count == 0)
                {
                    Msg = Msg = "-Please Fill Atleast One Payables Details!!\r\n";
                }
            }

            if (Msg != string.Empty)
            {
                MessageBox.Show(Msg, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                Valid = false;
            }

            return Valid;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            ModeC.Mode = "Load";
            EnableDisableButtons();
            ClearPageControls();
        }

        private void BtnExport_Click(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            if (DGridPayablesDtls.DataContext != null)
            {
                DataTable Dt = (DGridPayablesDtls.DataContext as DataView).Table;
                if (Dt.Rows.Count > 0)
                {
                    bcom.ExportToExcel(DGridPayablesDtls);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using MPS.DLayer;
using MPS.Library;

namespace MPS.UserControls
{
    /// <summary>
    /// Interaction logic for FindControl.xaml
    /// </summary>
    public partial class FindControl : UserControl
    {
        public FindControl()
        {
            InitializeComponent();
        }

        #region DependencyProperties
        public static readonly DependencyProperty BindtableProperty = DependencyProperty.Register("Bindtable", typeof(DataTable), typeof(FindControl));

        public DataTable Bindtable
        {
            get { return (DataTable)GetValue(BindtableProperty); }
            set { SetValue(BindtableProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(DataRowView), typeof(FindControl));

        public DataRowView SelectedItem
        {
            get { return (DataRowView)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }
        #endregion

        public event EventHandler OnSelectionChanged;

        private void FeeGrd_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (FeeGrd.SelectedItem != null)
            {
                if (this.OnSelectionChanged != null)
                {
                    this.OnSelectionChanged(this, e);
                }
            }
        }

        public void FillFind(string Tag)
        {
            dcommon dcom = new dcommon();
            DataSet ds = null;
            DataTable Table;
            string Query = "";

            object obj = dcom.Execute("select * from setup.Lookup where LookupCode='" + Tag.ToUpper() + "'");
            if ((obj) is DataSet)
            {
                ds = (DataSet)obj;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Query = Convert.ToString(ds.Tables[0].Rows[0]["LookupQuery"]);

                    #region Organization,Period...... Filters
                    Query = Query.ToUpper().Replace("#ORGID#", CurrentSession.OrgID.ToSafeString());
                    Query = Query.ToUpper().Replace("#ACCORGID#", CurrentSession.AccOrgID.ToSafeString());
                    Query = Query.ToUpper().Replace("#FYID#", CurrentSession.PeriodID.ToSafeString());
                    #endregion

                    obj = dcom.Execute(Query);
                    if ((obj) is DataSet)
                    {
                        ds = (DataSet)obj;

                        if (ds.Tables.Count > 0)
                        {
                            Table = ds.Tables[0];
                            FeeGrd.DataContext = Table;
                        }

                    }
                    else
                    {
                        return;
                    }
                }

            }
            else
            {
                return;
            }

        }

        private void FindControl1_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void FindControl1_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void FeeGrd_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header.ToString().ToUpper().Trim() == "ID")
            {
                e.Column.Visibility = Visibility.Hidden;
            }

            e.Column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);

        }

        private void FeeGrd_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = true;
                FeeGrd_MouseLeftButtonDown(null, null);
            }
        }

        private void FeeGrd_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            if (FeeGrd.IsKeyboardFocusWithin == false)
            {
                Keyboard.Focus(FeeGrd);
                FeeGrd.SelectedIndex = 0;
                //FeeGrd.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
                this.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
            }
        }

        #region Search
        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataTable Dt = FeeGrd.DataContext as DataTable;
            if (Dt.DefaultView.Sort.Trim() != "")
            {
                string[] Columns = Dt.DefaultView.Sort.Split(',');

                Dt.DefaultView.RowFilter = GenerateFilterString(Columns, txtSearch.Text);
            }
            else
            {
                if (Dt.DefaultView.Count == 0 && Dt.Rows.Count > 0)    //************** If Sort Persent But Not Visible
                {
                    Dt.DefaultView.RowFilter = "";
                    txtSearch_TextChanged(null, null);
                }
                else                                                  //************** If No Sorting Done
                {
                    string[] Cols = new string[Dt.Columns.Count];
                    for (int i = 0; i < Dt.Columns.Count; i++)
                    {
                        Cols[i] = "[" + Dt.Columns[i].ColumnName + "]";
                    }
                    Dt.DefaultView.RowFilter = GenerateFilterString(Cols, txtSearch.Text);
                }
            }
        }

        private string GenerateFilterString(string[] Columns, string Text)
        {
            string Filter = "";

            foreach (string col in Columns)
            {
                string ConvertedCol = "Convert(" + col + ",System.String)";

                if (Filter.Trim() == "")
                {
                    Filter = ConvertedCol + " Like '%" + Text + "%'";
                }
                else
                {
                    Filter = Filter + " or " + ConvertedCol + " Like '%" + Text + "%'";
                }
            }

            return Filter;
        }
        #endregion
    }
}

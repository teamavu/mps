﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MPS.DLayer;
using MPS.BLayer;
using MPS.Library;
using System.Data;
using System.Data.SqlClient;
using CustomControls;

namespace MPS.BLayer
{
    class bPeriod
    {
        dcommon dcom = new dcommon();
        dPeriod dper = new dPeriod();

        public object Update(libPeriod lib, string mode)
        {
            return dper.Update(lib, mode);
        }

        public object Retrieve(int Period_Id = 0)
        {
            return dper.Retrive(Period_Id);
        }
    }
}

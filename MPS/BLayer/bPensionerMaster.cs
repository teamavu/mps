﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MPS.DLayer;
using MPS.Library;

namespace MPS.BLayer
{
    public class bPensionerMaster
    {
        dPensionerMaster dclass = new dPensionerMaster();
        public object Retrieve(int id)
        {
            return dclass.Retrieve(id);
        }
        public object Update(LibPensionerMaster lib, string Mode)
        {
            return dclass.Update(lib, Mode);
        }
        public DataTable GetPensionerType(int Ptype_Id)
        {
            dcommon dcom = new dcommon();
            object obj = null;
            DataTable dt = null;
            string str = "select * from BaseV.PensionerType where Pt_Deleted=0 and Pt_Id=" + Ptype_Id + "";
            obj = dcom.Execute(str);
            if (obj is DataSet)
            {
                DataSet ds = (DataSet)obj;
                dt = ds.Tables[0];
            }
            else
            {
                throw new Exception(obj.ToString());
            }
            return dt;

        }

        public Boolean CheckDuplicateAdharNo(int id, string AdharNo)
        {
            DataTable dt = dclass.CheckDuplicateAdharNo(id, AdharNo);
            bool HasFill = true;
            int count = 0;


            if (dt.Rows.Count > 0)
            {
                count = dt.Rows[0]["Count"].ToSafeInteger();
            }
            if (count > 0)
            {
                HasFill = false;
            }
            else
            {
                HasFill = true;
            }


            return HasFill;
        }

        public Boolean DuplicatePanNo(int id, string PanNo)
        {
            DataTable dt = dclass.DuplicatePanNo(id, PanNo);
            bool HasFill = true;
            int count = 0;


            if (dt.Rows.Count > 0)
            {
                count = dt.Rows[0]["Count"].ToSafeInteger();
            }
            if (count > 0)
            {
                HasFill = false;
            }
            else
            {
                HasFill = true;
            }


            return HasFill;
        }

        public Boolean DuplicateVoterId(int id, string VoterId)
        {
            DataTable dt = dclass.DuplicateVoterId(id, VoterId);
            bool HasFill = true;
            int count = 0;


            if (dt.Rows.Count > 0)
            {
                count = dt.Rows[0]["Count"].ToSafeInteger();
            }
            if (count > 0)
            {
                HasFill = false;
            }
            else
            {
                HasFill = true;
            }


            return HasFill;
        }

        public Boolean DuplicateDriverLicNo(int id, string DriverLicNo)
        {
            DataTable dt = dclass.DuplicateDriverLicNo(id, DriverLicNo);
            bool HasFill = true;
            int count = 0;


            if (dt.Rows.Count > 0)
            {
                count = dt.Rows[0]["Count"].ToSafeInteger();
            }
            if (count > 0)
            {
                HasFill = false;
            }
            else
            {
                HasFill = true;
            }


            return HasFill;
        }

        public Boolean DuplicateMembershipNo(int id, string MembrshipNo)
        {
            DataTable dt = dclass.DuplicateMembershipNo(id, MembrshipNo);
            bool HasFill = true;
            int count = 0;


            if (dt.Rows.Count > 0)
            {
                count = dt.Rows[0]["Count"].ToSafeInteger();
            }
            if (count > 0)
            {
                HasFill = false;
            }
            else
            {
                HasFill = true;
            }


            return HasFill;
        }


        public Boolean DuplicateRefNo(int id, int Type, string RefNo)
        {
            DataTable dt = dclass.DuplicateRefNo(id, Type,RefNo);
            bool HasFill = true;
            int count = 0;


            if (dt.Rows.Count > 0)
            {
                count = dt.Rows[0]["Count"].ToSafeInteger();
            }
            if (count > 0)
            {
                HasFill = false;
            }
            else
            {
                HasFill = true;
            }


            return HasFill;
        }

       
    }
}

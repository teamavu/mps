﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MPS.DLayer;
using MPS.Library;

namespace MPS.BLayer
{
    public class bPayables
    {
        dPayables dClass = new dPayables();

        public object CreateEmptyTable()
        {
            return dClass.CreateEmptyTable();
        }

        public object BindGrid(int PeriodId, int TypeId)
        {
            return dClass.BindGrid(PeriodId, TypeId);
        }

        public int Update(LibPayables Lib, string Mode, out int Id, out string Msg)
        {
            return dClass.Update(Lib, Mode, out Id, out Msg);
        }
    }
}

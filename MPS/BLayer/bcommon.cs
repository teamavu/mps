﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.IO;
using System.Diagnostics;
using System.Windows.Media;
using MPS.DLayer;
using MPS;
using MPS.Library;
using CustomControls;
using System.Threading;
using EXX = Microsoft.Office.Interop.Excel;


namespace MPS.BLayer
{

    public class bcommon
    {
        dcommon dcom = new dcommon();
        public void FillCustomControl(TextBoxSearch TxtSearch, string LOVCode, string ConditionValue1, string ConditionValue2, string AddQuery)
        {
            DataSet dbSet = new DataSet();
            try
            {
                string mStr = "";

                mStr = "select * from SETUP.LOVList WHERE isnull(pLOV_Active,0)=1 and  pLOV_Code= '" + LOVCode + "'";
                object obj = dcom.Execute(mStr);
                if (obj is DataSet)
                {
                    dbSet = (DataSet)obj;
                    if (dbSet.Tables[0].Rows.Count > 0)
                    {
                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_SQL"].ToString()) == false)
                            mStr = dbSet.Tables[0].Rows[0]["pLOV_SQL"].ToString().ToUpper();

                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString()) == false && (int)MySession.Current.BusinessGroupId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString() + " = " + (int)MySession.Current.BusinessGroupId;
                        //    else
                        //        mStr = mStr + " WHERE " + dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString() + " = " + (int)MySession.Current.BusinessGroupId;
                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString()) == false && (int)MySession.Current.OrganizationId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString() + " = " + (int)MySession.Current.OrganizationId;
                        //    else
                        //        mStr = mStr + " WHERE " + dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString() + " = " + (int)MySession.Current.OrganizationId;

                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString()) == false && (int)MySession.Current.BudgetYearId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString() + " = " + (int)MySession.Current.BudgetYearId;
                        //    else
                        //        mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString() + " = " + (int)MySession.Current.BudgetYearId;

                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString()) == false && ConditionValue1 != "")
                            if (mStr.IndexOf("WHERE") > -1)
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "STRING")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "DATE")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = " + ConditionValue1;
                            else
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "STRING")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "DATE")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = " + ConditionValue1;


                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString()) == false && ConditionValue2 != "")
                            if (mStr.IndexOf("WHERE") > -1)
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "NUMBER" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                            else
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "NUMBER" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;

                        if (AddQuery != "" && dbSet.Tables[0].Rows[0]["pLOV_AddSql"].ToString() == "1")
                        {
                            if (mStr.IndexOf("WHERE") > -1)
                                mStr = mStr + " and " + AddQuery;
                            else
                                mStr = mStr + " where " + AddQuery;
                        }
                        if (Convert.ToInt32(dbSet.Tables[0].Rows[0]["pLOV_OrderBy"].ToString()) > 0)
                        {
                            mStr = mStr + " Order By " + dbSet.Tables[0].Rows[0]["pLOV_OrderBy"].ToString();
                        }

                    }
                }
                obj = dcom.Execute(mStr);
                if (obj is DataSet)
                {
                    dbSet = (DataSet)obj;
                    DataTable dt = dbSet.Tables[0];
                    TxtSearch.ItemsSource = dt;                                  
                    dbSet.Dispose();
                }
            }
            catch (Exception ex)
            {
                if (dbSet.Tables.Count != 0)
                {
                    dbSet.Dispose();
                }
                throw (ex);
            }
        }        

        public void FillLOVDDL(ComboBox ddl, string LOVCode, string ConditionValue1, string ConditionValue2, string AddQuery)
        {
            DataSet dbSet = new DataSet();
            try
            {
                string mStr = "";

                mStr = "select * from SETUP.LOVList WHERE isnull(pLOV_Active,0)=1 and  pLOV_Code= '" + LOVCode + "'";
                object obj = dcom.Execute(mStr);
                if (obj is DataSet)
                {
                    dbSet = (DataSet)obj;
                    if (dbSet.Tables[0].Rows.Count > 0)
                    {
                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_SQL"].ToString()) == false)
                            mStr = dbSet.Tables[0].Rows[0]["pLOV_SQL"].ToString().ToUpper();

                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString()) == false && (int)MySession.Current.BusinessGroupId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString() + " = " + (int)MySession.Current.BusinessGroupId;
                        //    else
                        //        mStr = mStr + " WHERE " + dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString() + " = " + (int)MySession.Current.BusinessGroupId;
                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString()) == false && (int)MySession.Current.OrganizationId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString() + " = " + (int)MySession.Current.OrganizationId;
                        //    else
                        //        mStr = mStr + " WHERE " + dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString() + " = " + (int)MySession.Current.OrganizationId;

                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString()) == false && (int)MySession.Current.BudgetYearId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString() + " = " + (int)MySession.Current.BudgetYearId;
                        //    else
                        //        mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString() + " = " + (int)MySession.Current.BudgetYearId;

                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString()) == false && ConditionValue1 != "")
                            if (mStr.IndexOf("WHERE") > -1)
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "STRING")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "DATE")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = " + ConditionValue1;
                            else
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "STRING")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "DATE")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = " + ConditionValue1;


                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString()) == false && ConditionValue2 != "")
                            if (mStr.IndexOf("WHERE") > -1)
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "NUMBER" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                            else
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "NUMBER" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;

                        if (AddQuery != "" && dbSet.Tables[0].Rows[0]["pLOV_AddSql"].ToString() == "1")
                        {
                            if (mStr.ToUpper().Contains(" WHERE ") == true)
                                mStr = mStr + " and " + AddQuery;
                            else
                                mStr = mStr + " where " + AddQuery;
                        }
                        if (Convert.ToInt32(dbSet.Tables[0].Rows[0]["pLOV_OrderBy"].ToString()) > 0)
                        {
                            mStr = mStr + " Order By " + dbSet.Tables[0].Rows[0]["pLOV_OrderBy"].ToString();
                        }

                    }
                }
                obj = dcom.ExecuteDataSet(mStr);
                if (obj is DataSet)
                {
                    dbSet = (DataSet)obj;
                    DataTable dt = dbSet.Tables[0];
                    DataRow dr = dt.NewRow();
                    dr[0] = "--select--";
                    dr[1] = "0";
                    dt.Rows.InsertAt(dr, 0);

                    ddl.DataContext = dt;
                    ddl.SelectedValuePath = dt.Columns[1].ColumnName;
                    ddl.DisplayMemberPath = dt.Columns[0].ColumnName;

                    ddl.SelectedValue = 0;
                    dbSet.Dispose();
                }
            }
            catch (Exception ex)
            {
                if (dbSet.Tables.Count != 0)
                {
                    dbSet.Dispose();
                }
                throw (ex);
            }
        }

        public void FillDataGridComboBoxLOVDDL(DataGridComboBoxColumn ddl, string LOVCode, string ConditionValue1, string ConditionValue2, string AddQuery)
        {
            DataSet dbSet = new DataSet();
            try
            {
                string mStr = "";

                mStr = "select * from SETUP.LOVList WHERE isnull(pLOV_Active,0)=1 and  pLOV_Code= '" + LOVCode + "'";
                object obj = dcom.Execute(mStr);
                if (obj is DataSet)
                {
                    dbSet = (DataSet)obj;
                    if (dbSet.Tables[0].Rows.Count > 0)
                    {
                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_SQL"].ToString()) == false)
                            mStr = dbSet.Tables[0].Rows[0]["pLOV_SQL"].ToString().ToUpper();

                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString()) == false && (int)MySession.Current.BusinessGroupId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString() + " = " + (int)MySession.Current.BusinessGroupId;
                        //    else
                        //        mStr = mStr + " WHERE " + dbSet.Tables[0].Rows[0]["pLOV_BGColumnName"].ToString() + " = " + (int)MySession.Current.BusinessGroupId;
                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString()) == false && (int)MySession.Current.OrganizationId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString() + " = " + (int)MySession.Current.OrganizationId;
                        //    else
                        //        mStr = mStr + " WHERE " + dbSet.Tables[0].Rows[0]["pLOV_OrgColumnName"].ToString() + " = " + (int)MySession.Current.OrganizationId;

                        //if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString()) == false && (int)MySession.Current.BudgetYearId > 0)
                        //    if (mStr.IndexOf("WHERE") > -1)
                        //        mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString() + " = " + (int)MySession.Current.BudgetYearId;
                        //    else
                        //        mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FyColumnName"].ToString() + " = " + (int)MySession.Current.BudgetYearId;

                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString()) == false && ConditionValue1 != "")
                            if (mStr.IndexOf("WHERE") > -1)
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "STRING")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "DATE")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = " + ConditionValue1;
                            else
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "STRING")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn1DataType"].ToString() == "DATE")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = '" + ConditionValue1 + "' ";
                                else
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName1"].ToString() + " = " + ConditionValue1;


                        if (string.IsNullOrEmpty(dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString()) == false && ConditionValue2 != "")
                            if (mStr.IndexOf("WHERE") > -1)
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "NUMBER" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " or " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else
                                    mStr = mStr + " and " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                            else
                                if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "AND")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "NUMBER" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "STRING" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else if (dbSet.Tables[0].Rows[0]["pLOV_FilterColumn2DataType"].ToString() == "DATE" && dbSet.Tables[0].Rows[0]["pLOV_JoinOperator"].ToString() == "OR")
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = '" + ConditionValue2 + "' ";
                                else
                                    mStr = mStr + " where " + dbSet.Tables[0].Rows[0]["pLOV_FilterColumnName2"].ToString() + " = " + ConditionValue2;

                        if (AddQuery != "" && dbSet.Tables[0].Rows[0]["pLOV_AddSql"].ToString() == "1")
                        {
                            if (mStr.ToUpper().Contains(" WHERE ") == true)
                                mStr = mStr + " and " + AddQuery;
                            else
                                mStr = mStr + " where " + AddQuery;
                        }
                        if (Convert.ToInt32(dbSet.Tables[0].Rows[0]["pLOV_OrderBy"].ToString()) > 0)
                        {
                            mStr = mStr + " Order By " + dbSet.Tables[0].Rows[0]["pLOV_OrderBy"].ToString();
                        }

                    }
                }
                obj = dcom.ExecuteDataSet(mStr);
                if (obj is DataSet)
                {
                    dbSet = (DataSet)obj;
                    DataTable dt = dbSet.Tables[0];
                    DataRow dr = dt.NewRow();
                    dr[0] = "--select--";
                    dr[1] = "0";
                    dt.Rows.InsertAt(dr, 0);
                    if (dt.Rows.Count > 0)
                    {
                        ddl.ItemsSource = dt.DefaultView;
                        ddl.SelectedValuePath = dt.Columns[1].ColumnName;
                        ddl.DisplayMemberPath = dt.Columns[0].ColumnName;
                    }                   
                    dbSet.Dispose();
                }
            }
            catch (Exception ex)
            {
                if (dbSet.Tables.Count != 0)
                {
                    dbSet.Dispose();
                }
                throw (ex);
            }
        }

        public void FillListBox(ListBox List, string Tag, string Filter1, string Filter2)
        {
            DataSet ds = null;
            DataTable Table;
            string Query = "";

            object obj = dcom.Execute("select * from Master.LovList where Lov_Tag='" + Tag.ToUpper() + "'and Lov_Deleted=0");

            if ((obj) is DataSet)
            {
                ds = (DataSet)obj;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Query = Convert.ToString(ds.Tables[0].Rows[0]["Lov_SqlQuery"]);

                    if (ds.Tables[0].Rows[0]["Lov_FilterCol1"].ToString() != "")
                    {
                        Query = Query + " and " + ds.Tables[0].Rows[0]["Lov_FilterCol1"].ToString() + "='" + Filter1 + "'";
                    }

                    if (ds.Tables[0].Rows[0]["Lov_FilterCol2"].ToString() != "")
                    {
                        Query = Query + " and " + ds.Tables[0].Rows[0]["Lov_FilterCol2"].ToString() + "='" + Filter2 + "'";
                    }

                    obj = dcom.Execute(Query);

                    if ((obj) is DataSet)
                    {
                        ds = (DataSet)obj;

                        if (ds.Tables.Count > 0)
                        {
                            Table = ds.Tables[0];

                            if (Table.Rows.Count > 0)
                            {
                                List.DisplayMemberPath = Table.Columns[1].ToString();
                                List.SelectedValuePath = Table.Columns[0].ToString();
                                List.DataContext = Table;
                                List.SelectedIndex = -1;
                            }
                        }

                    }
                    else
                    {
                        return;
                    }
                }

            }
            else
            {
                return;
            }

        }

        public BitmapImage RetriveImageSource(byte[] ImageBytes)
        {
            try
            {
                if (ImageBytes != null)
                {
                    BitmapImage Bitimg = new BitmapImage();

                    Bitimg.BeginInit();

                    byte[] data = ImageBytes;

                    System.IO.MemoryStream strm = new System.IO.MemoryStream();

                    strm.Write(data, 0, data.Length);

                    strm.Position = 0;

                    Bitimg.StreamSource = strm;

                    Bitimg.EndInit();

                    return Bitimg;
                }

                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }

        }

        public DataTable GetGridTable(DataGrid Grid)
        {
            DataTable Dt = new DataTable();
            object Obj = Grid.ItemsSource;
            if (Obj != null)
            {
                DataView Dv = (DataView)Obj;
                Dt = Dv.ToTable();
            }
            return Dt;
        }

        public void OpenFile(string Path)
        {
            Process.Start(Path.Trim());
        }

        public byte[] GetBytesFromImage(Image ImageToConvert)
        {
            BitmapImage ImageSource = ImageToConvert.Source as BitmapImage;
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(ImageSource));
            encoder.Save(memStream);
            return memStream.GetBuffer();
        }

        public List<string> OpenFileBrowser(bool Multiselect = true)
        {
            List<string> Filenames = new List<string>();
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.InitialDirectory = "C:\\";
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.Multiselect = Multiselect;
            dlg.ShowDialog();
            foreach (string Filename in dlg.FileNames)
            {
                Filenames.Add(Filename);
            }
            return Filenames;
        }      

        public Boolean MultiDuplicateCheck(string str)
        {
            object obj = dcom.Execute(str);

            if (obj is DataSet)
            {
                DataSet ds = (DataSet)obj;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Already '" + FieldName + "' Defined !!!!')", true);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        public Page GetDocumentFromTag(string Tag)
        {
            OpenFile(Tag);
            return new Page();
        }

        public void ShowModalDialog(Grid PopupGrid, object Owner)
        {
            var Parent = PopupGrid.Parent;
            Grid Maingrid = new Grid();

            if (Parent is Grid)
            {
                Maingrid = Parent as Grid;
                Maingrid.Children.Remove(PopupGrid);
                ModalDialogBox ModBox = new ModalDialogBox(PopupGrid);
                ModBox.Owner = Owner as Window;
                ModBox.Maingrid.Children.Add(PopupGrid);
                PopupGrid.Visibility = Visibility.Visible;
                ModBox.ShowDialog();
            }

        }

        public Window GetparentWindow(UIElement Element)
        {
            object obj = null;

            while (Element != null && (Element is Window == false))
            {
                obj = VisualTreeHelper.GetParent(Element);

                if (obj == null)
                {
                    obj = LogicalTreeHelper.GetParent(Element);

                    if (obj == null)
                    {
                        return null;
                    }
                }

                Element = obj as UIElement;
            }

            return Element as Window;
        }

        public string GenerateDocumentNo(string MasterCode)
        {
            string DocNo = "";

            object obj = dcom.Execute("exec [SPGet].pGetDocumentNo @MastCode='" + MasterCode + "', @Org_Id=" + CurrentSession.OrgID + ", @Bg_Id=" + CurrentSession.BgId + ", @Fy_Id=" + CurrentSession.PeriodID);
            if (obj is DataSet)
            {
                DataSet Ds = (DataSet)obj;
                if (Ds.Tables.Count > 0)
                {
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        DocNo = Ds.Tables[0].Rows[0][0].ToString();
                    }
                }
            }
            return DocNo;
        }
      
        public List<string> OpenFileBrowser()
        {
            List<string> Filenames = new List<string>();
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.InitialDirectory = "C:\\";
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.Multiselect = true;
            dlg.ShowDialog();
            foreach (string Filename in dlg.FileNames)
            {
                Filenames.Add(Filename);
            }
            return Filenames;
        }

        public byte[] GetFileData(string FilePath)
        {
            FileInfo Finf = new FileInfo(FilePath);
            int Filesize=Convert.ToInt32(Finf.Length);
            byte[] FileData = new byte[Filesize];
            using (FileStream Fstream = new FileStream(FilePath, FileMode.Open,FileAccess.Read))
            {
                Fstream.Read(FileData, 0, Filesize);
                return FileData;
            }
                       
        }

        public void DeleteTempFiles()
        {
            DirectoryInfo Dirinf = new DirectoryInfo(Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/Temp/");
            foreach (FileInfo Fileinf in Dirinf.GetFiles())
            {
                try
                {
                    Fileinf.Delete();                    
                }
                catch(Exception Ex)
                {                    
                    
                }
            }
        }

        public string RetriveFileFromDatabase(string FileName, byte[] Data, string SaveLocation = null)
        {
            if (Data == null)
            {
                throw new Exception("Byte Data Cannot Be Null.");
            }

            if (SaveLocation == null)
            {
                SaveLocation = CurrentSession.TemporaryFilesLocation + FileName;
            }

            using (FileStream Fs = new FileStream(SaveLocation, FileMode.Create))
            {
                Fs.Write(Data, 0, Data.Length);
            }
            return SaveLocation;
        }

        public string SaveFile(string FileName, byte[] Data, string Filters = "")
        {
            string ReturnLocation = null;
            Microsoft.Win32.SaveFileDialog SaveDlg = new Microsoft.Win32.SaveFileDialog();
            SaveDlg.FileName = FileName;
            SaveDlg.Filter = Filters + "All files (*.*)|*.*";
            if (SaveDlg.ShowDialog() == true)   //When Pressed Save
            {
                RetriveFileFromDatabase(FileName, Data, SaveDlg.FileName);
                ReturnLocation = Directory.GetParent(SaveDlg.FileName).FullName;
            }
            return ReturnLocation;
        }

        public int GetSeqNo(string tablename ,string ProcedureName)
        {
            int seqno = 0;
            object obj = dcom.Execute("Execute " + ProcedureName + " '" + tablename + "'");
            if (obj is DataSet)
            {
                DataSet ds = (DataSet)obj;
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        seqno = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                    }
                }
            }
            return seqno;
        }

        public bool ValidateDateInput(string TransType, DateTime InputDate)
        {
            bool IsValid = true;

            #region FinancialYear Check
            if (InputDate < CurrentSession.PeriodFrom || InputDate > CurrentSession.PeriodTo)
            {
                return false;
            } 
            #endregion

            DateTime CutOffDay,StartDateLimit, EndDateLimit, Today = DateTime.Today;           
            DataSet Ds = dcom.ExecuteDataSet("exec [SpGet].[faGetDateValidations] @fadv_Transactiontype='" + TransType + "', @fadv_OrgId=" + CurrentSession.OrgID);
            DataTable TblValidations = Ds.Tables[0];
            if (TblValidations.Rows.Count > 0)
            {
                #region CutOffDay
                if (TblValidations.Rows[0]["fadv_CutOffDay"].ToSafeInteger() > 0)
                {
                    CutOffDay = new DateTime(Today.Year, Today.Month, TblValidations.Rows[0]["fadv_CutOffDay"].ToSafeInteger());
                }
                else
                {
                    CutOffDay = Today;
                }
                #endregion

                #region EndDateLimit
                if (TblValidations.Rows[0]["fadv_CutIsIncluded"].ToSafeInteger() == 1)
                {
                    EndDateLimit = Today;
                }
                else
                {
                    EndDateLimit = CutOffDay;
                }
                #endregion

                #region StartDateTime
                if (Today.CompareTo(CutOffDay) <= 0)
                {
                    StartDateLimit = GetStartDateLimitForValidation(TblValidations, "ACTIVE");
                }
                else
                {
                    StartDateLimit = GetStartDateLimitForValidation(TblValidations, "CUTOFF");
                } 
                #endregion  

                if (InputDate.Date.CompareTo(StartDateLimit.Date) >= 0 && InputDate.Date.CompareTo(EndDateLimit.Date) <= 0)
                {
                    IsValid = true;
                }
                else
                {
                    IsValid = false;
                }
            }

            #region Exceptions
            if (IsValid == false)
            {
                DateTime ExceptionDateFrom = new DateTime(1900, 1, 1);
                DateTime ExceptionDateTill = new DateTime(1900, 1, 1);

                if (TblValidations.Rows[0]["Excep_From"].ToString().Trim() != "" && TblValidations.Rows[0]["Excep_Till"].ToString().Trim() != "")
                {
                    ExceptionDateFrom = Convert.ToDateTime(TblValidations.Rows[0]["Excep_From"]);
                    ExceptionDateTill = Convert.ToDateTime(TblValidations.Rows[0]["Excep_Till"]);
                }
                else if (TblValidations.Rows[0]["Excep_From"].ToString().Trim() != "")
                {
                    ExceptionDateFrom = Convert.ToDateTime(TblValidations.Rows[0]["Excep_From"]);
                    ExceptionDateTill = Today;
                }
                else if (TblValidations.Rows[0]["Excep_Till"].ToString().Trim() != "")
                {
                    ExceptionDateFrom = Today;
                    ExceptionDateTill = Convert.ToDateTime(TblValidations.Rows[0]["Excep_Till"]);
                }               

                if (InputDate.Date.CompareTo(ExceptionDateFrom.Date) >= 0 && InputDate.Date.CompareTo(ExceptionDateTill.Date) <= 0)
                {
                    IsValid = true;
                }
                else
                {
                    IsValid = false;
                }
            } 
            #endregion

            return IsValid;
        }

        private DateTime GetStartDateLimitForValidation(DataTable TableValidations,string ApplyRuleParameter)
        {
            byte CurrentMonth = 0;
            int PeriodToAllow = 0;
            int ExactPeriodDay = 0;
            string PeriodUOM = "";
            DateTime StartDateLimit, Today = DateTime.Today;

            switch (ApplyRuleParameter.ToUpper())
            {
                case "ACTIVE":
                    {
                        CurrentMonth =Convert.ToByte(TableValidations.Rows[0]["fadv_CurrentMonth"]);
                        PeriodToAllow = TableValidations.Rows[0]["fadv_PeriodToAllow"].ToSafeInteger();
                        ExactPeriodDay = TableValidations.Rows[0]["fadv_PeriodExactDay"].ToSafeInteger();
                        PeriodUOM = TableValidations.Rows[0]["fadv_PeriodUOM"].ToString().Trim().ToUpper();
                        break;
                    }
                case "CUTOFF":
                    {
                        CurrentMonth = Convert.ToByte(TableValidations.Rows[0]["fadv_CutCurrentMonth"]);
                        PeriodToAllow = TableValidations.Rows[0]["fadv_CutPeriodToAllow"].ToSafeInteger();
                        ExactPeriodDay = TableValidations.Rows[0]["fadv_CutPeriodExactDay"].ToSafeInteger();
                        PeriodUOM = TableValidations.Rows[0]["fadv_CutPeriodUOM"].ToString().Trim().ToUpper();
                        break;
                    }                   
            }

            #region CurrentMonth
            if (CurrentMonth == 1)
            {
                StartDateLimit = new DateTime(Today.Year, Today.Month, 1);
            }
            else
            {
                StartDateLimit = Today;
            } 
            #endregion

            #region Period
            if ((PeriodToAllow) > 0)
            {
                switch (PeriodUOM)
                {
                    case "DAYS":
                        {
                            StartDateLimit = StartDateLimit.AddDays((-1) * PeriodToAllow);
                            break;
                        }
                    case "MONTHS":
                        {
                            StartDateLimit = StartDateLimit.AddMonths((-1) * PeriodToAllow);
                            break;
                        }
                    case "YEARS":
                        {
                            StartDateLimit = StartDateLimit.AddYears((-1) * PeriodToAllow);
                            break;
                        }
                }
            }
            #endregion

            #region PeriodExactDay
            if (ExactPeriodDay > 0)
            {
                StartDateLimit = new DateTime(StartDateLimit.Year, StartDateLimit.Month, ExactPeriodDay);
            } 
            #endregion

            return StartDateLimit;
        }

        public string GetTransactionTag(int TransTypeID)
        {
            string TAG = "";
            DataTable Dt = CurrentSession.TransTypeTable.Copy();
            var Rows = Dt.AsEnumerable().Where(z => z.Field<int>("ftt_Id") == TransTypeID);
            if (Rows.Count() > 0)
            {
                TAG = Rows.ElementAt(0).Field<string>("ftt_Tag").ToSafeString();
            }
            return TAG;
        }

        public DataTable GetButtonsPanelAccessRights(string PageTag)
        {
            DataTable Dt = null;
            object obj = dcom.Execute("[SpGet].[faButtonsPanelAccessRights] @User_Id=" + CurrentSession.UserId + ",@Page_Tag='" + PageTag + "'");
            if (obj is DataSet)
            {
                DataSet Ds = (DataSet)obj;
                Dt = Ds.Tables[0];
            }
            return Dt;
        }

        public string GetTextFromComboBox(ComboBox DDL)
        {
            string DDLText = "";
            if (DDL.SelectedValue.ToSafeInteger() > 0)
            {
                DataView Dv = DDL.ItemsSource as DataView;
                if (Dv != null)
                {
                    var Rows = Dv.Table.AsEnumerable().Where(z => z.Field<object>(1).ToString() == DDL.SelectedValue.ToString());
                    if (Rows.Count() > 0)
                    {
                        DDLText = (Rows.ElementAt<DataRow>(0) as DataRow)[0].ToString();
                    }
                }
            }
            return DDLText;
        }

        public void OpenPageInNewWindow(string PageTag,Window ParentWindow)
        {
            string Tag = PageTag;
            SecondaryWindow SecWin = new SecondaryWindow();
            SecWin.FrmSecondary.Navigate(GetPage(Tag));
            SecWin.Owner = ParentWindow;
            SecWin.Show();
        }

        public DataTable ReseqTable(DataTable Table, string SeqColname)
        {
            int i = 1;

            foreach (DataRow dr in Table.Rows)
            {
                dr[SeqColname] = i++;
            }

            return Table;
        }

        public void ProcessExistsCheck()
        {
            Process CurrentProcess = Process.GetCurrentProcess();
            if (Process.GetProcessesByName(CurrentProcess.ProcessName).Count() > 1)
            {
                if (MessageBox.Show("Another instance of " + CurrentProcess.ProcessName + " is already running!\r\nDo you want to restart " + CurrentProcess.ProcessName + "?", "Already Running", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var query = Process.GetProcessesByName(CurrentProcess.ProcessName).Where(z => z.Id != CurrentProcess.Id);
                    if (query.Count() > 0)
                    {
                        Process SecondProcess = query.ElementAt(0);
                        SecondProcess.Kill();
                    }
                }
                else
                {
                    CurrentProcess.Kill();
                }
            }

        }

        public Page GetPage(string Tag)
        {
            Page Pg = new Page();

            if (Tag.Trim().ToUpper() == "QUERY")
            {
                return new QUERY() { Tag = Tag.Trim().ToUpper() };
            }
            else if (Tag.Trim().ToUpper() == "PAYABLES")
            {
                return new Payables() { Tag = Tag.Trim().ToUpper() };
            }
            else if(Tag.Trim().ToUpper()=="PENSIONERMASTER")
            {
                return new PensionerMaster() { Tag = Tag.Trim().ToUpper() };
            }
            else if (Tag.Trim().ToUpper() == "PERIODMASTER")
            {
                return new PeriodMaster() { Tag = Tag.Trim().ToUpper() };
            }
            else if(Tag.Trim().ToUpper()== "PAYMENTRETURN")
            {
                return new PaymentReturn() { Tag = Tag.Trim().ToUpper() }; 
            }
            else if (Tag.Trim().ToUpper() == "PAYMENT")
            {
                return new Payment() { Tag = Tag.Trim().ToUpper() }; 
            }
            else
            {
                return new Page();
            }
        }

        #region FACosting
        public object GetFACostingPlant(string TransFromDate, string TransToDate)
        {
            try
            {
                DataSet Dset = null;

                string Sql = "Exec [Report].[pFACostingReportPlant] @TransFromDate='" + DateTime.Parse(TransFromDate).ToString("yyyy/MM/dd") + "',@TransToDate='" + DateTime.Parse(TransToDate).ToString("yyyy/MM/dd") + "'";
                object obj = dcom.Execute(Sql);
                if (obj is DataSet)
                {
                    Dset = (DataSet)obj;
                    return Dset;
                }
                else
                {
                    return (string)obj;
                }
            }
            catch (Exception Ex)
            {
                string msG = Ex.ToString();
                return msG;
            }
        }
        #endregion FACosting

        #region FAFinalAccounts
        public object GetFAFinalAccounts(string TransFromDate, string TransToDate)
        {
            try
            {
                DataSet Dset = null;

                string Sql = "Exec [SPGet].[GetFATrialBalance] @TransFromDate='" + DateTime.Parse(TransFromDate).ToString("yyyy/MM/dd") + "',@TransToDate='" + DateTime.Parse(TransToDate).ToString("yyyy/MM/dd") + "'";
                object obj = dcom.Execute(Sql);
                if (obj is DataSet)
                {
                    Dset = (DataSet)obj;
                    return Dset;
                }
                else
                {
                    return (string)obj;
                }
            }
            catch (Exception Ex)
            {
                string msG = Ex.ToString();
                return msG;
            }
        }
        #endregion FAFinalAccounts

        #region FAMonthClosedCheck
        public bool MonthClosed(string Transdate,out string ErrorMsg)
        {
            ErrorMsg = string.Empty;
            if (CurrentSession.IsPeriodClosed)
            {
                ErrorMsg = "Period " + CurrentSession.PeriodFrom.Year + "-" + CurrentSession.PeriodTo.Year + " is Closed!";
                return true;
            }

            if (Transdate == string.Empty)
            {
                return false;
            }

            if (DateTime.Compare(CurrentSession.PeriodFrom, Convert.ToDateTime(Transdate)) > 0 || DateTime.Compare(CurrentSession.PeriodTo, Convert.ToDateTime(Transdate)) < 0)
            {
                ErrorMsg = "Transaction Date is outside current Period!";
                return true;
            }

            DataTable Dt = dcom.ExecuteDatatable("exec [SPGet].[FaGetMonthClosedStatus] @OrgId = " + CurrentSession.OrgID + ",@TransDate = '" + Convert.ToDateTime(Transdate).ToString("MM/dd/yyyy") + "'");
            if (Dt.Rows[0]["MonthStatus"].ToString().ToUpper() == "CLOSE")
            {
                ErrorMsg = "Transaction Month is Closed!";                
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        public void DDLSetDefaults(ComboBox DDL) /* LovList Requires a column '_Default' */
        {
            DataView Dv = DDL.ItemsSource as DataView;
            if (Dv.Table.Columns.Cast<DataColumn>().Any(z => z.ColumnName.Trim().ToUpper() == "_DEFAULT"))
            {
                for (int i = 0; i < Dv.Count; i++)
                {
                    if (Dv[i]["_DEFAULT"].ToSafeInteger() > 0)
                    {
                        DDL.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        #region EXCEL CODE
        public void ExportToExcel(UIElement Element, string SelectionType = "ALL")
        {
            if (Element is DataGrid)
            {
                DataGrid Dg = (DataGrid)Element;

                /*if (SelectionType.ToUpper() == "SELECTED")
                {

                }*/

                Dg.SelectAllCells();


                Dg.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, Dg);

                try
                {
                    SetExcelStyle(Dg);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception");
                }
                finally
                {
                    // Dg.UnselectAllCells();
                }
            }
        }

        public void SetExcelStyle(DataGrid Dg)
        {
            int ExcelColWidth = 10;
            int ExeclColHeight = 50;
            Microsoft.Office.Interop.Excel.Application Excl = new Microsoft.Office.Interop.Excel.Application();
            object Misval = System.Reflection.Missing.Value;
            Microsoft.Office.Interop.Excel.Workbook Wrkb = Excl.Workbooks.Add(Misval);
            Microsoft.Office.Interop.Excel.Sheets Wrksheets = Wrkb.Sheets;
            Microsoft.Office.Interop.Excel.Worksheet Wrksht = Wrksheets[1];
            Wrksht.Paste();

            Wrksht.Columns.AutoFit();
            Microsoft.Office.Interop.Excel.Style StylHdr = Wrkb.Styles.Add("Header");
            Microsoft.Office.Interop.Excel.Style Styl = null;
            EXX.Range ImageRng = null;
            EXX.Range DateFormatRng = null;
            EXX.Range RowStylRng = null;
            EXX.Range HdrStylRng = Wrksht.get_Range("A1");
            EXX.Range WidthWrapRng = null;

            try
            {
                DataView DefaultView = (Dg.DataContext as DataView);
                DataTable ImageTable = DefaultView.ToTable().Clone();

                #region Filter Images For Selected CellRows
                //Dg.SelectAll();
                var DistinctRowViews = Dg.SelectedCells.Select(z => z.Item).Distinct();
                foreach (DataRowView RowView in DistinctRowViews)
                {
                    ImageTable.ImportRow(RowView.Row);
                }
                #endregion

                //#region Hide Select Column
                //var StrColumns = DefaultView.ToTable().Columns.Cast<DataColumn>().Where(z => z.DataType.Name.ToString().ToUpper() == "INT32");

                //foreach (DataColumn Col in StrColumns)
                //{
                //    int ColIndex = GetColIndexFromExcel(Wrksht, Col);
                //    string ColName = Col.ColumnName.ToString();
                //    if (ColName == "IsSelected")
                //    {
                //        Dg.Columns[0].Visibility = Visibility.Hidden;
                //    }
                //}
                //#endregion


                #region Image To Excel

                var ImgColumns = DefaultView.ToTable().Columns.Cast<DataColumn>().Where(z => z.DataType.Name.ToString().ToUpper() == "BYTE[]" && (z.ColumnName.ToUpper().Contains("IMG") || z.ColumnName.ToUpper().Contains("IMAGE") || z.ColumnName.ToUpper().Contains("LOGO")));
                foreach (DataColumn Col in ImgColumns)
                {
                    int ColIndex = GetColIndexFromExcel(Wrksht, Col);
                    if (ColIndex == 0)
                    {
                        continue;
                    }
                    int RowCount = 2;
                    foreach (DataRow dr in ImageTable.Rows)
                    {
                        string CellAddress = Convert.ToString(Wrksht.Cells[RowCount, ColIndex].Address);
                        ImageRng = Wrksht.get_Range(CellAddress);
                        ImageRng.RowHeight = ExeclColHeight;
                        ImageRng.ColumnWidth = ExcelColWidth;
                        System.Drawing.Image Image = byteToImage(dr[Col.ColumnName.ToString()] as byte[]);
                        if (Image != null)
                        {
                            var DataObj = new DataObject(DataFormats.Bitmap, Image, true);
                            var bmp = DataObj.GetData("System.Drawing.Bitmap") as System.Drawing.Bitmap;
                            using (System.Drawing.Bitmap bmpToSave = new System.Drawing.Bitmap(600, 600))   //********** To Generate Square Images.
                            {
                                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmpToSave))
                                {
                                    g.DrawImage(bmp, 2, 2, bmpToSave.Width, bmpToSave.Height);
                                    System.Windows.Forms.Clipboard.SetImage(bmpToSave);
                                }

                                Wrksht.Paste(ImageRng);
                            }
                        }


                        #region RowStyle
                        string RowAddress = Convert.ToString(Wrksht.Cells[RowCount, 1].Address);
                        RowStylRng = Wrksht.get_Range(RowAddress);
                        RowStylRng = RowStylRng.get_Resize(1, ImageTable.Columns.Count);
                        Styl = Wrkb.Styles.Add("Rows" + ColIndex + RowCount);
                        Styl.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                        RowStylRng.Style = Styl;
                        #endregion

                        RowCount++;
                    }

                }

                #endregion

                #region Header Style
                StylHdr.Font.Color = System.Drawing.Color.Red;
                StylHdr.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                StylHdr.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                HdrStylRng = HdrStylRng.get_Resize(1, Dg.Columns.Count);
                HdrStylRng.Style = StylHdr;
                #endregion

                #region Date Fromat
                //********Filter Datetime columns VISIBLE in Datagrid.
                var columns = DefaultView.ToTable().Columns.Cast<DataColumn>().Where(z => z.DataType.Name.ToString().ToUpper() == "DATETIME" && Dg.Columns.Any(p => p.Visibility == Visibility.Visible && p.Header.ToString() == z.ColumnName.ToString()));
                foreach (DataColumn Col in columns)
                {
                    int ColIndex = GetColIndexFromExcel(Wrksht, Col);
                    if (ColIndex > 0)
                    {
                        string CellAddress = Convert.ToString(Wrksht.Cells[2, ColIndex].Address);
                        DateFormatRng = Wrksht.get_Range(CellAddress);
                        DateFormatRng = DateFormatRng.get_Resize(DefaultView.ToTable().Rows.Count, 1);
                        DateFormatRng.NumberFormat = "dd-MM-yy";
                    }
                }
                #endregion

                #region Wrap And Width
                string StartCellAddressWidth = Convert.ToString(Wrksht.Cells[1, 1].Address);
                WidthWrapRng = Wrksht.get_Range(StartCellAddressWidth);
                WidthWrapRng = WidthWrapRng.get_Resize(DefaultView.ToTable().Rows.Count + 1, DefaultView.ToTable().Columns.Count);
                WidthWrapRng.ColumnWidth = ExcelColWidth;
                WidthWrapRng.WrapText = true;
                #endregion

                #region Resize PictureBox
                foreach (EXX.Shape Shp in Wrksht.Shapes)
                {
                    if (Shp.Name.Contains("Picture"))
                    {
                        Shp.Height = 50;
                    }
                }
                #endregion

                Excl.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception");
            }
            finally
            {
                if (StylHdr != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(StylHdr); }
                if (Styl != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Styl); }
                if (ImageRng != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ImageRng); }
                if (DateFormatRng != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(DateFormatRng); }
                if (RowStylRng != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(RowStylRng); }
                if (HdrStylRng != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(HdrStylRng); }
                if (WidthWrapRng != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(WidthWrapRng); }
                if (Wrksht != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Wrksht); }
                if (Wrksheets != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Wrksheets); }
                if (Wrkb != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Wrkb); }
                if (Excl != null) { System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Excl); }
            }
        }

        private int GetColIndexFromExcel(Microsoft.Office.Interop.Excel.Worksheet WSheet, DataColumn DCol)
        {
            int Count = 0;
            DataTable BaseTable = DCol.Table;
            for (int i = 1; i <= BaseTable.Columns.Count; i++)
            {
                object cell = WSheet.Cells[1, i].Address;
                if (DCol.ColumnName.ToString() == WSheet.get_Range(cell).get_Value())
                {
                    Count = i;
                    break;
                }
            }
            return Count;
        }

        public System.Drawing.Image byteToImage(byte[] RawData)
        {
            System.Drawing.Image Image = null;
            if (RawData != null)
            {
                MemoryStream ms = new MemoryStream(RawData, 0, RawData.Count());
                try
                {
                    Image = System.Drawing.Bitmap.FromStream(ms);
                }
                catch
                {
                }
            }
            return Image;
        }
        #endregion
    }

}

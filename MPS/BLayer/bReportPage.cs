﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MPS.Library;
using MPS.DLayer;

namespace MPS.BLayer
{
   public class bReportPage
    {
       public DataTable GetQueryResult(String str)
       {
           dcommon obj = new dcommon();
           DataTable dt = obj.ExecuteDatatable(str);
           return dt;
       }


       public DataTable GetReportList()
       {
           dcommon obj = new dcommon();
           DataTable dt = obj.ExecuteDatatable("exec SpGet.pGetReportList @OrgId=" +CurrentSession.OrgID + ",@ModId=7,@UsrId="+CurrentSession.UserId );
           return dt;


       }
       public DataTable GetParameterList(String RepId)
       {
           dcommon obj = new dcommon();
           DataTable dt = obj.ExecuteDatatable("select rsp_ID,rsp_RepID,rsp_DisplayName,rsp_TAG,rsp_SerialOrder,rsp_LovSourceSQL,rsp_LovSourceSQLFilter,rsp_SourceColumnToFilter,rsp_SourceColumnDataType,rsp_ReportColumnToFilter,rsp_OperatorToCompare,rsp_AllowGroupBy,rsp_DateInputType,rsp_DateInputFormat,rsp_MandatoryFlag,rsp_OrgFilter,rsp_OrgFlag,rsp_ParFlag,rsp_ParName1,rsp_ParName2,rsp_ParInputType,rsp_IsDefaultDate,rsp_IsProcedureParameter from SETUP.ReportSelectionParameter where rsp_deleted=0 and rsp_RepID=" + RepId);
           return dt;
       }
       public DataTable GetParameterValues(String query)
       {
           dcommon obj = new dcommon();
           DataTable dt = obj.ExecuteDatatable(query);
           return dt;
       }

       public DataTable GetReportDetials(string RepId)
       {
           dcommon obj = new dcommon();
           DataTable dt = obj.ExecuteDatatable("select Rep_ID,Rep_ReportSourceSQL ,case when ISNULL(repe_reportfilename,'')='' then Rep_ReportFileName else repe_reportfilename end Rep_ReportFileName,Rep_ReportSource,Rep_DisplayName,ISNULL(up_poprn,'View') up_poprn,Rep_QueryInProcedureRepFlag  from basev.reportlist left outer join BaseV.ReportExeption on repe_repid=Rep_ID and repe_deleted=0 and repe_orgid=" + CurrentSession.OrgID + " left outer join BaseV.UserPermission on UP_PageId=Rep_ID and UP_ModId=15 and UP_Type='Report' and up_usrId= " + CurrentSession.UserId + " and UP_poprn='Print' where rep_id=" + RepId);
           return dt;

       }

       public DataSet CreateEmpltyDetails(string fathid, string PayeeAccId, string ReceiptType)
       {
           dcommon Drep = new dcommon();
           DataSet ds = Drep.ExecuteDataSet("exec [SPGet].[pGetfaDirectReceipt] @fathId=" + fathid + ",@PayeeAccId=" + PayeeAccId + ",@fath_FYId=" + CurrentSession.PeriodID + ",@fath_Orgid=" + CurrentSession.OrgID + ",@ReceiptType='" + ReceiptType + "'");
           return ds;
       }

       public object GetReportDesc(long RepId)
       {
           try
           {
               dcommon dcom = new dcommon();
               string Sql = "exec [SpGet].[pGetReportDescription] @Rep_Id=" + RepId + "";
               object obj = dcom.Execute(Sql);
               return obj;
           }
           catch (Exception ex)
           {
               return ex.Message;
           }
       }
    }
}

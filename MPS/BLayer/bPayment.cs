﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MPS.DLayer;
using MPS.Library;

namespace MPS.BLayer
{
    public class bPayment
    {
        dPayment dClass = new dPayment();

        public object CreateEmptyTable()
        {
            return dClass.CreateEmptyTable();
        }

        public object BindGrid(int PeriodId, int TypeId,int PaymentMethod=0)
        {
            return dClass.BindGrid(PeriodId, TypeId,PaymentMethod);
        }

        public DataTable Retrieve(int Payables_Id)
        {
            DataTable dt = new DataTable();
            object obj= dClass.Retrieve(Payables_Id);
            if(obj is DataSet)
            {
                DataSet ds = (DataSet)obj;
                dt = ds.Tables[0];
            }
            else
            {
                throw new Exception(obj.ToString());
            }
            return dt;
        }

        public int Update(LibPayment Lib, string Mode, out int Id, out string Msg)
        {
            return dClass.Update(Lib, Mode,out Id,out Msg);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using System.Diagnostics;
using MPS.DLayer;
using MPS.Library;

namespace MPS.BLayer
{    
    public class bMasterWindow
    {
        dcommon dcom = new dcommon();        

        public void SetConnectionState(string State)
        {
            //if (State.ToUpper().Trim() == "OPEN")
            //{
            //    dcom.OpenConnection();
            //}
            //else if (State.ToUpper().Trim() == "CLOSE")
            //{
            //    dcom.CloseConnection();
            //}
        }
        public DataTable GetMenuItemsTable()
        {
            DataTable Dt = new DataTable();
            object obj = dcom.Execute("[SPGet].[GetFaPageAccessNF]  @ProjectName='MPS',@User_Id=" + CurrentSession.UserId);
            if (obj is DataSet)
            {
                if (((DataSet)obj).Tables.Count > 0)
                {
                    Dt = ((DataSet)obj).Tables[0];
                }
            }
            return Dt;
        }
        public DataTable GetMenuTable()
        {
            DataTable Dt = new DataTable();                
            
            object obj = dcom.Execute("Select Menu_Id,Menu_Name from baseV.BISMenu where Menu_Deleted=0 and Menu_IsInvisible=0 and Menu_ProjectTag='" + CurrentSession.ProjectName + "'");
            if (obj is DataSet)
            {
                if (((DataSet)obj).Tables.Count > 0)
                {
                    Dt = ((DataSet)obj).Tables[0];                                    
                }

            }
            return Dt;
        }

        public DataTable GetMenuItemsTable(int MenuId)
        {
            DataTable Dt = new DataTable();
            object obj = dcom.Execute("[SpGet].[GetFaPageAccess] @MItem_MenuId=" + MenuId + ",@ProjectName='Finance',@User_Id=" + CurrentSession.UserId);
            if (obj is DataSet)
            {
                if (((DataSet)obj).Tables.Count > 0)
                {
                    Dt = ((DataSet)obj).Tables[0];
                }
            }
            return Dt;
        }

    }
}

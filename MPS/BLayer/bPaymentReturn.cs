﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MPS.DLayer;
using MPS.Library;
using System.Data.SqlClient;

namespace MPS.BLayer
{
    public class bPaymentReturn
    {
        dPaymentReturn dClass = new dPaymentReturn();

        public object CreateEmptyTable()
        {
            return dClass.CreateEmptyTable();
        }

        public object BindGrid(long PensionerId)
        {
            return dClass.BindGrid(PensionerId);
        }

        public int Update(LibPaymentReturn Lib, string Mode, out int Id, out string Msg)
        {
            return dClass.Update(Lib, Mode, out Id, out Msg);
        }
    }
}

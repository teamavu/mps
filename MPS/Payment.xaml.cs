﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using MPS.BLayer;
using MPS.DLayer;
using MPS.Library;

namespace MPS
{
    /// <summary>
    /// Interaction logic for Payment.xaml
    /// </summary>
    public partial class Payment : Page
    {
        bcommon bcom = new bcommon();
        dcommon dcom = new dcommon();
        bPayment bClass = new bPayment();
        dPayment dClass = new dPayment();
        LibPayment Lib = new LibPayment();
        ModeClass ModeC = new ModeClass();
        bool IsFirstLoad = true;

        public Payment()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsFirstLoad)
            {
                IsFirstLoad = false;
                UIServices.SetBusyState();
                ModeC.Mode = "Load";
                ClearPageControls();
                LoadDDL();
                CreateEmptyTable();
                EnableDisableButtons();
            }
        }

        public bool ValidateToSave()
        {
            string Msg = string.Empty; bool Valid = true;
            
            if (Lib.PaymentDetails == null)
            {
                Msg = Msg = "-Please Fill Atleast One Payables Details!!\r\n";
            }

            if (Lib.PaymentDetails != null)
            {
                if (Lib.PaymentDetails.Rows.Count == 0)
                {
                    Msg = Msg = "-Please Fill Atleast One Payables Details!!\r\n";
                }
            }
           // DataView dv = DGridPayablesDtls.DataContext as DataView;
           // dv.RowFilter = "Payables_PaidFlag=1";
           //// Lib.PaymentDetails = dv.ToTable();
           // if (dv.Count == 0)
           // {
           //     Msg = Msg = "-Please Check Atleast One Payables Details!!\r\n";
           // }

            if (Lib.PaymentDetails != null)
            {
                if (Lib.PaymentDetails.Rows.Count > 0)
                {
                    foreach (DataRow dr in Lib.PaymentDetails.Rows)
                    {
                        if (dr["Payables_PaidFlag"].ToSafeInteger()==1)
                        {
                            if(dr["Payables_Date"].ToString()==string.Empty || dr["Payables_RefNo"].ToString()==string.Empty)
                            {
                                Msg = Msg = "-Please Enter Date and RefNo of "+dr["PMast_Name"] +"!!\r\n";
                            }
                        }
                    }
                   
                }
            }
            if (Msg != string.Empty)
            {
                MessageBox.Show(Msg, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                Valid = false;
            }

            return Valid;
        }

        public void ClearPageControls()
        {
            txtid.Text = "0";
            txtmessage.Text = string.Empty;
            ddlPeriod.SelectedValue = 0;
            ddlType.SelectedValue = 0;
            ddlPaymentMethod.SelectedValue = 0;
            DGridPayablesDtls.DataContext = null;
            ddlPeriod.Focus();
        }


        public void LoadDDL()
        {
            if (ddlPeriod.SelectedValue == 0) { bcom.FillCustomControl(ddlPeriod, "PayablesPeriod", "", "", ""); }
            if (ddlType.SelectedValue == 0) { bcom.FillCustomControl(ddlType, "PayablesType", "", "", ""); }
            if (ddlPaymentMethod.SelectedValue == 0) { bcom.FillCustomControl(ddlPaymentMethod, "PaymentType", "", "", ""); }
        }

        public bool ValidateToFilter()
        {
            string Msg = string.Empty; bool Valid = true;
            if (ddlPeriod.SelectedValue == 0)
            {
                Msg = Msg + "-Please Select Period!!\r\n";
            }

            if (ddlType.SelectedValue == 0)
            {
                Msg = Msg + "-Please Select Type!!\r\n";
            }
            //if(ddlPaymentMethod.SelectedValue==0)
            //{
            //    Msg = Msg + "-Please Select Payment Method!!\r\n";
            //}
            if (Msg != string.Empty)
            {
                MessageBox.Show(Msg, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                if (Msg.Contains("Period"))
                {
                    ddlPeriod.Focus();
                }
                else if (Msg.Contains("Type"))
                {
                    ddlType.Focus();
                }
                else if (Msg.Contains("Method"))
                {
                    ddlPaymentMethod.Focus();
                }
                Valid = false;
            }
            return Valid;
        }

        public void CreateEmptyTable()
        {
            try
            {
                object obj = bClass.CreateEmptyTable();
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if (ds.Tables.Count > 0)
                    {
                        Lib.PaymentDetails = ds.Tables[0];
                        DGridPayablesDtls.DataContext = Lib.PaymentDetails.DefaultView;
                        Lib.PaymentDetails.DefaultView.RowFilter = "Payables_Deleted=0";
                    }
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
        }

        public void BindGrid()
        {
            try
            {
                object obj = bClass.BindGrid(ddlPeriod.SelectedValue.ToSafeInteger(), ddlType.SelectedValue.ToSafeInteger(),ddlPaymentMethod.SelectedValue.ToSafeInteger());
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if (ds.Tables.Count > 0)
                    {
                        Lib.PaymentDetails = ds.Tables[0];
                        DGridPayablesDtls.DataContext = Lib.PaymentDetails.DefaultView;
                        Lib.PaymentDetails.DefaultView.RowFilter = "Payables_Deleted=0";
                    }
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        public void EnableDisableButtons()
        {
            if (ModeC.Mode == "Load")
            {
                BtnSave.IsEnabled = false;
                BtnCancel.IsEnabled = true;
                BtnFind.IsEnabled = true;
            }
            else if (ModeC.Mode == "Edit")
            {
                BtnSave.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnFind.IsEnabled = true;
            }
        }

        private void BtnFilter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ValidateToFilter() == false)
                {
                    return;
                }

                BindGrid();
                ModeC.Mode = "Edit";
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int Id = 0; string Msg = string.Empty;

                if (ValidateToSave() == false)
                {
                    return;
                }
               else if(ValidateToFilter()==false)
                {
                    return;
                }
                Lib.Payables_PeriodId = ddlPeriod.SelectedValue.ToSafeInteger();
                Lib.Payables_TypeId = ddlType.SelectedValue.ToSafeInteger();
                DataView dv = DGridPayablesDtls.DataContext as DataView;
                dv.RowFilter = "Payables_PaidFlag=1";
                Lib.PaymentDetails = dv.ToTable();
                txtid.Text = bClass.Update(Lib, ModeC.Mode, out Id, out Msg).ToString();
                if (Id == 0)
                {
                    txtmessage.Text = Msg;
                    return;
                }

                txtmessage.Text = Msg;
                ModeC.Mode = "Load";
                BindGrid();
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            ModeC.Mode = "Load";
            EnableDisableButtons();
            ClearPageControls();
        }

        private void BtnFind_Click(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            Find.FillFind("Payment");
            FindPopup.IsOpen = true;
        }

        public void Retrieve(int id)
        {
            try
            {
                DataTable dt = bClass.Retrieve(id);
                if(dt !=null)
                {
                    if(dt.Rows.Count > 0)
                    {
                        foreach(DataRow dr in dt.Rows)
                        {
                            ddlType.SelectedValue = dr["Payables_TypeId"].ToSafeInteger();
                            ddlPeriod.SelectedValue = dr["Payables_PeriodId"].ToSafeInteger();
                            ddlPaymentMethod.SelectedValue = dr["PMast_PaymentMethod"].ToSafeInteger();
                        }
                        Lib.PaymentDetails = dt;
                        DGridPayablesDtls.DataContext = Lib.PaymentDetails.DefaultView;
                        Lib.PaymentDetails.DefaultView.RowFilter = "Payables_Deleted=0";
                        ModeC.Mode = "Edit";
                        EnableDisableButtons();
                    }
                }
            }
            catch(Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
        }

        private void Find_OnSelectionChanged(object sender, EventArgs e)
        {
            try
            {
                txtmessage.Text = string.Empty;
                FindPopup.IsOpen = false;
                DataRowView Drv = Find.SelectedItem;
                txtid.Text = Convert.ToString(Drv.Row["ID"]);
                Retrieve(txtid.Text.ToSafeInteger());
                //ddlType.SelectedValue = Drv.Row["Payables_TypeId"].ToSafeInteger();
                //ddlPeriod.SelectedValue = Drv.Row["Payables_PeriodId"].ToSafeInteger();
                //BindGrid();
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
           
        }

        //private void txtCheckPayable_Checked(object sender, RoutedEventArgs e)
        //{
            
        //}
    }
}

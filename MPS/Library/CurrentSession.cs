﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;

namespace MPS.Library
{
    public class CurrentSession
    {      
        public static int UserId { get; set; }
        public static string UserName { get; set; }
        public static int BgId { get; set; }
        public static int OrgID { get; set; }
        public static int fapar_IsAutoChequeNo { get; set; }
        public static string OrgName { get; set; }
        public static int AccOrgID { get; set; }
        public static int PeriodID { get; set; }   
        public static int CurrencyId { get; set; }
        public static int RoundOffLedgerID { get; set; }
        public static string RoundOffLedgerName { get; set; }
        public static bool IsPeriodClosed { get; set; }

        public static DateTime PeriodFrom { get; set; }
        public static DateTime PeriodTo { get; set; } 
        public static string DateFormat { get; set; }
        public static DataTable UserOrgList { get; set; }   // Columns -: Name,ID
        public static DataTable TransTypeTable { get; set; }

        public static string TemporaryFilesLocation = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/Temp/";
        public static string ProjectName = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;

        public static void ResetSession()
        {
            UserId = 0;
            UserName = string.Empty;
            BgId = 0;
            OrgID = 0;
            AccOrgID = 0;
            PeriodID = 0;
            CurrencyId = 0;
            IsPeriodClosed = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace MPS.Library
{
    public class FontClass
    {
        static double ImproveValue = 0.7;
        public static void ChangeFontSize(object Object)
        {
            DependencyObject DepObj = Object as DependencyObject;
            if (DepObj != null)
            {
                if (DepObj.GetType() == typeof(TextBlock))
                {
                    ((TextBlock)DepObj).FontSize = ((TextBlock)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(TextBox))
                {
                    ((TextBox)DepObj).FontSize = ((TextBox)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(Button))
                {
                    ((Button)DepObj).FontSize = ((Button)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(ComboBox))
                {
                    ((ComboBox)DepObj).FontSize = ((ComboBox)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(CheckBox))
                {
                    ((CheckBox)DepObj).FontSize = ((CheckBox)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(DataGrid))
                {
                    ((DataGrid)DepObj).FontSize = ((DataGrid)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(Label))
                {
                    ((Label)DepObj).FontSize = ((Label)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(TabItem))
                {
                    ((TabItem)DepObj).FontSize = ((TabItem)DepObj).FontSize + ImproveValue;
                }
                else if (DepObj.GetType() == typeof(RadioButton))
                {
                    ((RadioButton)DepObj).FontSize = ((RadioButton)DepObj).FontSize + ImproveValue;
                }
                foreach (object logicalChild in LogicalTreeHelper.GetChildren(DepObj))
                {
                    ChangeFontSize(logicalChild);
                }
            }
        }
    }
}

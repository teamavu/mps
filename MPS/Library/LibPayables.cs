﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MPS.Library
{
    public class LibPayables
    {
        public int Payables_Id { get; set; }

        public int Payables_PensionerId { get; set; }

        public int Payables_PeriodId { get; set; }

        public int Payables_TypeId { get; set; }

        public double Payables_Rate { get; set; }

        public double Payables_Amt { get; set; }

        public double Payables_MOC { get; set; }

        public string Payables_BankAccNo { get; set; }

        public string Payables_BankIFSCCode { get; set; }

        public DataTable PayablesDetails { get; set; }

    }
}

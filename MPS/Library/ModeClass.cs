﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.IO;

namespace MPS.Library
{
    public class ModeClass : INotifyPropertyChanged
    {
        private string _Mode;
        public string Mode
        {
            get
            {
                return _Mode;
            }
            set
            {
                _Mode = value;
                INotifyChanged("Mode");
            }
        }

        private DataTable _PermissionTable;
        public DataTable PermissionTable
        {
            get
            {
                return _PermissionTable;
            }
            set
            {
                _PermissionTable = value;
                INotifyChanged("PermissionTable");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void INotifyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }
}

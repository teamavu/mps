﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MPS.Library
{
  public  class libReportPage
    {
      public static string ReportQuery
      {
          get;
          set;
      }

      public static string ReportName
      {
          get;
          set;
      }

      public static string Rep_QueryInProcedureRepFlag
      {
          get;
          set;
      }

      public static DataTable ReportSourceData
      {
          get;
          set;
      }

      public static string ReportDisplayName
      {
          get;
          set;
      }

      public static DataTable ReportParamenter
      {
          get;
          set;
      }
      public static string ReportPrintorNot
      {
          get;
          set;
      }

      public DataTable ReportList
      {
          get;
          set;
      }
      public DataTable ReportParameter
      {
          get;
          set;
      }

      public DataTable ParameterValues
      {
          get;
          set;
      }

      public DataTable FilterValue
      {
          get;
          set;
      }
      
    }

  public static class ReportFilter
  {
      public const int LOvSQlQuery = 5;
      public const int ID = 0;
      public const int ParameterName = 2;
      public const int Value = 3;
      public const int ColumnName = 7;
      public const int DataType = 8;
      public const int DateInputType = 12;
      public const int rsp_OrgFilter = 15;
      public const int rsp_OrgFlag = 16;
      public const int rsp_MandatoryFlag = 14;
      public const int Operator =10;
      public const int rsp_IsDefaultDate = 21;
      public const int rsp_IsProcedureParameter = 22;
  }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPS.Library
{
    public class LibPensionerMaster
    {
       public int PMast_Id { get; set; }
        public string PMast_Name { get; set; }
        public string PMast_Address1{get;set;}
        public string PMast_Address2 { get; set; }
        public string PMast_Address3 { get; set; }
        public string PMast_Address4 { get; set; }
        public string PMast_PinNo { get; set; }
        public string PMast_RefNo { get; set; }
        public int PMast_Type { get; set; }
        public double PMast_MonthlyAmt { get; set; }
        public double PMast_MOC { get; set; }
        public int PMast_PaymentMethod { get; set; }
        public string PMast_AdharNo { get; set; }
        public string PMast_VoterIdNo { get; set; }
        public string PMast_PanNo { get; set; }
        public string PMast_DriveLicNo { get; set; }
        public string PMast_MobileNo { get; set; }
        public string PMast_OtherRefNo { get; set; }
        public string PMast_BankACNo { get; set; }
        public string PMast_BankBranch { get; set; }
        public string PMast_BankIFSCCode { get; set; }
        public string  PMast_Remark { get; set; }
        public string PMast_RenewOnMonth { get; set; }
        public string PMast_LastRenewOn { get; set; }
        public int PMast_InActive { get; set; }
        public string PMast_InActiveOn { get; set; }
        public string PMast_InActiveReason { get; set; }
        public string PMast_MembershipNo { get; set; }
        public string PMast_BankName { get; set; }

    }
}

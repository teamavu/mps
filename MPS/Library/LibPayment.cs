﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MPS.Library
{
    public class LibPayment
    {
        public int Payables_PeriodId { get; set; }
        public int Payables_Id { get; set; }
        public int Payables_TypeId { get; set; }
        public DataTable PaymentDetails { get; set; }
    }
}

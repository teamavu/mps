﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.IO;

namespace MPS.Library 
{
    public class LibMasterWindow : INotifyPropertyChanged
    {
        private DataTable _MenuTable;

        public DataTable MenuTable
        {
            get
            {
                return _MenuTable;
            }
            set
            {
                _MenuTable = value;
                INotifyChanged("MenuTable");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void INotifyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }
}

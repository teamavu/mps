﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MPS.Library
{
    public class LibPaymentReturn
    {
        public int Payables_Id { get; set; }

        public int Payables_PensionerId { get; set; }

        public string Payables_ReturnOnDate { get; set; }

        public string Payables_Reason { get; set; }

        public string Payables_ReturnComment { get; set; }

        public double Payables_ExpensesDeduct { get; set; }

        public DataTable PaymentRetrnDtls { get; set; }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using Data = System.Data;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;


namespace MPS.Library
{
    public static class ERBFunctions
    {
        public static string DisplayDate(string dateval)
        {
            string retVal = "";
            if (dateval != "")
            {
                int cPoint = dateval.IndexOf(" ");
                if (cPoint > 0)
                { dateval = dateval.Substring(0, cPoint); }
                DateTime thisDate1;
                thisDate1 = new DateTime();
                thisDate1 = DateTime.ParseExact(dateval, System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern, null);
                retVal = thisDate1.ToString(CurrentSession.DateFormat);
            }
            return retVal;
        }
        public static string RemoveTime(string dateval)
        {
            string retVal = "";
            if (dateval != "")
            {
                int cPoint = dateval.IndexOf(" ");
                if (cPoint > 0)
                { retVal = dateval.Substring(0, cPoint); }
            }
            return retVal;
        }
        public static string ConvertDbDate(string dateval)
        {
            string retVal = "";
            if (dateval != "")
            {
                try
                {
                    DateTime thisDate1;
                    thisDate1 = new DateTime();
                    thisDate1 = DateTime.ParseExact(dateval, CurrentSession.DateFormat, null);
                    retVal = thisDate1.ToString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern);
                }
                catch { retVal = ""; }
            }
            return retVal;
        }
        public static bool CompareDbDate(string date1, string date2, string opr)
        {
            DateTime thisDate1;
            DateTime thisDate2;
            thisDate1 = new DateTime();
            thisDate1 = DateTime.ParseExact(date1, System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern, null);
            thisDate2 = new DateTime();
            thisDate2 = DateTime.ParseExact(date2, System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern, null);
            switch (opr)
            {
                case "equalto":
                    return thisDate1 == thisDate2;
                case "greaterthan":
                    return thisDate2 > thisDate1;
                case "greaterthanequal":
                    return thisDate2 >= thisDate1;
                case "lessthan":
                    return thisDate2 < thisDate1;
                case "lessthanequal":
                    return thisDate2 <= thisDate1;
                default:
                    return true;
            }

        }
        public static string ToSafeString(this object obj)
        {
            return (obj ?? string.Empty).ToString();
        }

        public static bool ToSafeBool(this object obj)
        {
            bool retVal;
            retVal = obj == DBNull.Value ? false : (bool)obj;
            return retVal;
        }
        public static decimal ToSafeDecimal(this object obj)
        {
            if (obj != null)
            {
                if (obj.ToString() != "")
                {
                    return decimal.Parse(obj.ToString());
                }
                else
                {
                    return 0;
                }
            }
            else
            {              
                return 0;
            }

        }
        public static double ToSafeDouble(this object obj)
        {
            if (obj != null)
            {
                if (obj.ToString() != "")
                {
                    return double.Parse(obj.ToString());
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }
        public static int ToSafeInteger(this object obj)
        {
            if (obj != null)
            {
                if (obj.ToString() != "")
                {
                    return Int32.Parse(obj.ToString());
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }

        public static string dbString(string str)
        {
            str = str.Trim();
            str = str.Replace("\'", "\'\'");
            return str;
        }

        public static string ConvertToDate(string dateval)
        {
            string strDate = dateval;
            string[] arr = strDate.Split("/".ToCharArray());
            if (arr.Length > 2)
            {
                strDate = (string)(arr[1] + "/" + arr[0] + "/" + arr[2]);
            }
            return strDate;
        }
       
       

        /* Control Bindings */
      
        public static string DataTableToXML(DataTable dt)
        {
            StringBuilder retStr = new StringBuilder();
            retStr.Append("<?xml version='1.0'?><Products>");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                retStr.Append("<Record>");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    retStr.Append("<" + dt.Columns[j].ColumnName + ">");
                    //if (dt.Rows[i][j].ToString() == "")
                    //{
                    //    retStr.Append("null");
                    //}
                    //else
                    //{
                    retStr.Append(dt.Rows[i][j].ToString());
                    //}
                    retStr.Append("</" + dt.Columns[j].ColumnName + ">");
                }
                retStr.Append("</Record>");
            }
            retStr.Append("</Products>");
            return retStr.ToString();
        }

        public static bool IsDate(string input)
        {
            DateTime dt;
            return DateTime.TryParse(input, out dt);
        }
        public static Boolean IsNumeric(string stringToTest)
        {
            decimal result;
            return decimal.TryParse(stringToTest, out result);
        }
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }
        public static string ToSqlString(this object obj)
        {
            if (obj.ToSafeString().Trim() == "")
            {
                return string.Empty;
            }
            else
            {
                return obj.ToString().Replace("'", "''");
            }
        }
    }
}

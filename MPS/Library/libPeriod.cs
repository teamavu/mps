﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPS.Library
{
    class libPeriod
    {
        public int Period_Id{ get; set; }
        public string Period_Name { get; set; }
        public string Period_ShortName { get; set; }

        public string Period_FromDate { get; set; }
        public string Period_ToDate { get; set; }
        public int Period_InActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MPS.Library;
using MPS.BLayer;
using MPS.DLayer;
using System.Data.Common;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Data;

namespace MPS
{
    /// <summary>
    /// Interaction logic for FinMasterWindow.xaml
    /// </summary>
    public partial class MasterWindow : Window
    {
        bMasterWindow bclass = new bMasterWindow();
        bcommon bcom = new bcommon();
        LibMasterWindow lib = new LibMasterWindow();
        ModeClass ModeC = new ModeClass();
        DataTable DtLedgerPermission = new DataTable();
     
        public MasterWindow()
        {
            InitializeComponent();
            //SetEnvironmentVariables();
            Application.Current.MainWindow = this;
        }

      

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            LoadLabels();
            string Mode = string.Empty;
            DataTable dt = bclass.GetMenuItemsTable();
            if (dt.Rows.Count > 0)
            {
                ListPageMenu.DataContext = dt.DefaultView;

                this.Tag = "FA_LEDGER";
                ModeC.PermissionTable = bcom.GetButtonsPanelAccessRights(this.Tag.ToSafeString());
                //DtLedgerPermission = ModeC.PermissionTable;
                //if (DtLedgerPermission.Rows.Count > 0)
                //{
                //    foreach (DataRow dr in DtLedgerPermission.Rows)
                //    {
                //        Mode = dr["UP_poprn"].ToString();
                //        if (Mode == "View")
                //        {
                //            BtnLedger.IsEnabled = true;
                //        }
                //    }
                //}
                ListPageMenu.Focus();
            }
        
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBoxResult.No == MessageBox.Show("Do you want to Exit ?", "EXIT", MessageBoxButton.YesNo, MessageBoxImage.Question))
            {
                e.Cancel = true;
                return;
            }
        }
        private void LoadLabels()
        {
            txtOrgLabel.Text = CurrentSession.OrgName;
            txtLoginUser.Text = CurrentSession.UserName.ToUpper();
            txtPeriodLabel.Text = "(" + CurrentSession.PeriodFrom.Year + " - " + CurrentSession.PeriodTo.Year + ")";
        }
        //private void LstModules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    UIServices.SetBusyState();
        //    //if (LstModules.SelectedItems.Count > 0)
        //    //{
        //    //    DataRowView Drv = LstModules.SelectedItems[0] as DataRowView;
        //    //    DataTable dt = bclass.GetMenuItemsTable(Drv["Menu_Id"].ToSafeInteger());
        //    //    if (dt.Rows.Count > 0)
        //    //    {
        //    //        ListPageMenu.DataContext = dt.DefaultView;
        //    //        ListPageMenu.Focus();
        //    //    }
        //    //    else
        //    //    {
        //    //        ListPageMenu.DataContext = null;
        //    //    }
                
        //    //}
        //}

        private void ListPageMenu_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                NavigateToPage();
            }
        }
        private void NavigateToPage()
        {
            UIServices.SetBusyState();
            if (ListPageMenu.SelectedItems.Count > 0)
            {
                DataRowView Drv = ListPageMenu.SelectedItem as DataRowView;
                string PageTag = Drv["MItem_Tag"].ToString();
                string PageName = Drv["MItem_Name"].ToString();
                NavigatePageTabItem(PageTag, PageName);
            }
        }
        public void NavigatePageTabItem(string PageTag, string PageName, bool IsFromPageInfoControl = false)
        {
            if (MainTab.Items.Cast<TabItem>().Any(z => z.Tag.ToString() == PageTag))
            {
                TabItem TabItm = MainTab.Items.Cast<TabItem>().First(z => z.Tag.ToString() == PageTag);
                TabItm.IsSelected = true;
            }
            else
            {
                TabItem TabItm = new TabItem() { Header = PageName, Tag = PageTag, Style = this.FindResource("TabItm") as Style };
                Frame Frm = new Frame();
                Frm.ContentRendered += new EventHandler(Frm_ContentRendered);
                Frm.Navigating += new NavigatingCancelEventHandler(Frm_Navigating);
                Frm.Navigate(bcom.GetPage(PageTag));
                TabItm.Content = Frm;
                MainTab.Items.Add(TabItm);
                TabItm.IsSelected = true;
            }
        }
        private void NavigatePageTabItemFromPageInfo(string PageTag, string PageName)
        {
            if (MainTab.Items.Cast<TabItem>().Any(z => z.Tag.ToString() == PageTag))
            {
                TabItem TabItm = MainTab.Items.Cast<TabItem>().First(z => z.Tag.ToString() == PageTag);
                TabItm.IsSelected = true;
            }
            else
            {
                TabItem TabItm = new TabItem() { Header = PageName, Tag = PageTag, Style = this.FindResource("TabItm") as Style };
                Frame Frm = new Frame();
                Frm.ContentRendered += new EventHandler(Frm_ContentRendered);
                Frm.Navigating += new NavigatingCancelEventHandler(Frm_Navigating);
                Frm.Navigate(bcom.GetPage(PageTag));
                TabItm.Content = Frm;
                MainTab.Items.Add(TabItm);
                TabItm.IsSelected = true;
            }
        }
        public void PageInfoActionNavigate(Page Page_, string PageTag, string PageName)
        {
            TabItem TabItm = new TabItem() { Header = PageName, Tag = PageTag, Style = this.FindResource("TabItm") as Style };
            Frame Frm = new Frame();
            Frm.ContentRendered += new EventHandler(Frm_ContentRendered);
            Frm.Navigating += new NavigatingCancelEventHandler(Frm_Navigating);
            Frm.Navigate(Page_);
            TabItm.Content = Frm;
            MainTab.Items.Add(TabItm);
            TabItm.IsSelected = true;
        }
        private void Frm_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            UIServices.SetBusyState();
        }

        private void Frm_ContentRendered(object sender, EventArgs e)
        {
            #region RemoveBackEntry
            Frame Frm = ((Frame)sender);
            int a = 0;
            if (Frm.BackStack != null)
            {
                foreach (System.Windows.Navigation.JournalEntry K in Frm.BackStack)
                {
                    a = 0;
                    foreach (System.Windows.Navigation.JournalEntry K2 in Frm.BackStack)
                    {
                        if (K.Name == K2.Name)
                        {
                            a = a + 1;
                        }
                    }

                    if (a == 2)
                    {
                        Frm.RemoveBackEntry();
                        break;
                    }
                }
            }
            #endregion
        }

        private void ListPageMenu_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            NavigateToPage();
        }

        private void TabClose_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabItem TabItm = MainTab.Items.Cast<TabItem>().First(z => z.Tag.ToString() == ((Image)sender).Tag.ToString());
            MainTab.Items.Remove(TabItm);
        }

        private void TxtSearchmenu_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DataView dv = (DataView)ListPageMenu.ItemsSource;
                dv.RowFilter = "MItem_Name Like '%" + TxtSearchmenu.Text + "%'";
                ListPageMenu.ItemsSource = dv;
            }
           
        }

        private void btnDashboard_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Environment.SetEnvironmentVariable("#User#", CurrentSession.UserId.ToString());
            try
            {
                Process.Start(new dcommon().Getconfigdata("DashboardLocation"));
            }
            catch (Exception Ex)
            {
                if (Ex.Message.ToUpper().Contains("CANCELED") == false)
                {
                    throw Ex;
                }
            }
        }

        private void BtnColorScheme_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ColorPopup.IsOpen = true;
        }

        private void BtnCalculator_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Process.Start("calc.exe");
        }
        private void BorderColor_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            App.Current.Resources["ThemeColor"] = ((Border)sender).Background;
        }

        private void BtnMIS_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MISReports MIS = new MISReports();
            MIS.Owner = this;
            MIS.Show();   
        }
        
        private void BtnDashboard_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            Environment.SetEnvironmentVariable("#User#", CurrentSession.UserId.ToString());
            try
            {
                Process.Start(new dcommon().Getconfigdata("DashboardLocation"));
            }
            catch (Exception Ex)
            {
                if (Ex.Message.ToUpper().Contains("CANCELED") == false)
                {
                    throw Ex;

                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using MPS.BLayer;
using MPS.DLayer;
using MPS.Library;
using MPS.UserControls;

namespace MPS
{
    /// <summary>
    /// Interaction logic for PaymentReturn.xaml
    /// </summary>
    public partial class PaymentReturn : Page
    {
        bcommon bcom = new bcommon();
        dcommon dcom = new dcommon();
        bPaymentReturn bClass = new bPaymentReturn();
        dPaymentReturn dClass = new dPaymentReturn();
        LibPaymentReturn Lib = new LibPaymentReturn();
        ModeClass ModeC = new ModeClass();
        bool IsFirstLoad = true;

        public PaymentReturn()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsFirstLoad)
            {
                IsFirstLoad = false;
                UIServices.SetBusyState();
                ModeC.Mode = "Load";
                ClearPageControls();
                LoadDDL();
                CreateEmptyTable();
             //   EnableDisableButtons();
            }
        }

        public void ClearPageControls()
        {
            txtid.Text = string.Empty;
            txtmessage.Text = string.Empty;
            ddlPensioner.SelectedValue = 0;
            DGridPaymentReturn.DataContext = null;
        }

        public void ClearPopUpPageControls()
        {
            txtid.Text = string.Empty;
            txtmessage.Text = string.Empty;
            txtReturnedOn.Text = string.Empty;
            txtReason.Text = string.Empty;
            txtComments.Text = string.Empty;
            txtExpenseDeducted.Text = string.Empty;
        }

        public void LoadDDL()
        {
            if (ddlPensioner.SelectedValue == 0) { bcom.FillCustomControl(ddlPensioner, "PensionerName", "", "", ""); }
        }

        public void CreateEmptyTable()
        {
            try
            {
                object obj = bClass.CreateEmptyTable();
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if (ds.Tables.Count > 0)
                    {
                        Lib.PaymentRetrnDtls = ds.Tables[0];
                        DGridPaymentReturn.DataContext = Lib.PaymentRetrnDtls.DefaultView;
                        Lib.PaymentRetrnDtls.DefaultView.RowFilter = "Payables_Deleted=0";
                    }
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
        }

        public void EnableDisableButtons()
        {

        }

        public void BindGrid()
        {
            try
            {
                object obj = bClass.BindGrid(ddlPensioner.SelectedValue);
                if (obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    if (ds.Tables.Count > 0)
                    {
                        Lib.PaymentRetrnDtls = ds.Tables[0];
                        DGridPaymentReturn.DataContext = Lib.PaymentRetrnDtls.DefaultView;
                        Lib.PaymentRetrnDtls.DefaultView.RowFilter = "Payables_Deleted=0";
                    }
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnFilter_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnPaymentReturn_Click(object sender, RoutedEventArgs e)
        {
          
            DataRowView drv = DGridPaymentReturn.SelectedItem as DataRowView;
            if(drv !=null)
            {
                txtid.Text = drv["Payables_Id"].ToString();
                bcom.ShowModalDialog(PopupPaymentReturn, this);
            }
        }

        private void ddlPensioner_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ddlPensioner.SelectedValue > 0)
                {
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
                return;
            }
        }

        public bool ValidateToSave()
        {
            string Msg = string.Empty; bool Valid = true;
            if (txtReturnedOn.Text==string.Empty)
            {
                Msg = Msg + "-Please Enter Date!!\r\n";
            }

            if (txtReason.Text==string.Empty)
            {
                Msg = Msg + "-Please Enter Reason!!\r\n";
            }
           if(txtExpenseDeducted.Text==string.Empty)
            {
                Msg = Msg + "-Please Enter Expense Incurred and to be deducted!!\r\n";
            }
           if(txtComments.Text==string.Empty)
            {
                Msg = Msg + "-Please Enter Comment!!\r\n";
            }
            if (Msg != string.Empty)
            {
                MessageBox.Show(Msg, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                if (Msg.Contains("Date"))
                {
                    txtReturnedOn.Focus();
                }
                else if (Msg.Contains("Reason"))
                {
                    txtReason.Focus();
                }
                else if (Msg.Contains("Expense"))
                {
                    txtExpenseDeducted.Focus();
                }
                else if (Msg.Contains("Comment"))
                {
                    txtComments.Focus();
                }
                Valid = false;
            }
            return Valid;
        }

        private void BtnPopupSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int Id = 0; string Msg = string.Empty;
                ModeC.Mode = "Edit";
                if (ValidateToSave() == false)
                {
                    return;
                }

                Lib.Payables_Id = txtid.Text.ToSafeInteger();
                Lib.Payables_PensionerId = ddlPensioner.SelectedValue.ToSafeInteger();
                Lib.Payables_ReturnOnDate = txtReturnedOn.Text.ToString() == string.Empty ? "" : Convert.ToDateTime(txtReturnedOn.Text).ToString("yyyyMMdd");
                Lib.Payables_Reason = txtReason.Text;
                Lib.Payables_ExpensesDeduct = txtExpenseDeducted.Text.ToSafeDouble();
                Lib.Payables_ReturnComment = txtComments.Text;
                txtid.Text = bClass.Update(Lib, ModeC.Mode, out Id, out Msg).ToString();
                if (Id == 0)
                {
                    txtmessage.Text = Msg;
                    return;
                }
                ModeC.Mode = "Load";
                ClearPopUpPageControls();
                txtmessage.Text = Msg;
                BindGrid();
                bcom.GetparentWindow(PopupPaymentReturn).Close();

            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
        }

        private void BtnPopupCancel_Click(object sender, RoutedEventArgs e)
        {
            ModeC.Mode = "Load";
            ClearPopUpPageControls();
            bcom.GetparentWindow(PopupPaymentReturn).Close();
        }
    }
}

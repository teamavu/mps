﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MPS
{
    /// <summary>
    /// Interaction logic for ModalDialogBox.xaml
    /// </summary>
    public partial class ModalDialogBox : Window
    {
        private Grid PopupGrid = new Grid();

        public ModalDialogBox(Grid Grid)
        {
            InitializeComponent();
            PopupGrid = Grid; 
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Window MainWindow = Application.Current.MainWindow;
            MainWindow.Activated += new EventHandler(MainWindow_Activated);

            ScaleTransform Scale = new ScaleTransform();
            PopupGrid.RenderTransform = Scale;
            Scale.CenterX = SystemParameters.WorkArea.Width / 4;
            Scale.CenterY = SystemParameters.WorkArea.Height / 4;
            DoubleAnimation anim = new DoubleAnimation(0.6, 1, TimeSpan.FromMilliseconds(200));
            Scale.BeginAnimation(ScaleTransform.ScaleXProperty, anim);
            Scale.BeginAnimation(ScaleTransform.ScaleYProperty, anim);

            this.MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
        }

        void MainWindow_Activated(object sender, EventArgs e)
        {
            this.Activate();
        }
    }
}

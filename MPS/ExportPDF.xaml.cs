﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using SAPBusinessObjects.WPF.Viewer;
using CrystalDecisions.ReportSource;
using System.Windows.Controls.Primitives;
using SAPBusinessObjects.WPF.ViewerShared;
using System.IO;
using MPS.Library;
using MPS.BLayer;
using MPS.DLayer;
using System.Data;
using System.Data.SqlClient;
using CustomControls;


namespace MPS
{
    /// <summary>
    /// Interaction logic for ExportPDF.xaml
    /// </summary>
    public partial class ExportPDF : Window
    {
        public ExportPDF()
        {
            InitializeComponent();
        }
        ReportDocument report = new ReportDocument();
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                string timestamp = DateTime.Now.ToString("ddMMyyyy hhmmss");
                object ObjRPT = this.ConvertToPDF();
                if (ObjRPT is ReportDocument)
                {
                    report = (ReportDocument)ObjRPT;
                    report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;

                    report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, System.AppDomain.CurrentDomain.BaseDirectory + "Temp/" + libReportPage.ReportDisplayName + timestamp + ".pdf");


                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = System.AppDomain.CurrentDomain.BaseDirectory + "Temp/" + libReportPage.ReportDisplayName + timestamp + ".pdf";
                    proc.Start();
                    //proc.WaitForExit();
                    proc.Dispose();

                }
                else
                {
                    report.Dispose();
                    return;
                    //this.lblMessage.Text = (string)ObjRPT;
                }
            }
            catch
            {
                report.Dispose();
                return;
            }
        }

        protected object ConvertToPDF()
        {

            try
            {
                string ServerName = dcommon.ServerName;// System.Configuration.ConfigurationManager.AppSettings["RptServer"];
                string DatabaseName = "BizERP";// System.Configuration.ConfigurationManager.AppSettings["RptDatabase"];
                string UserId = "ba";// System.Configuration.ConfigurationManager.AppSettings["RptUser"];
                string Password = "sql2008";// System.Configuration.ConfigurationManager.AppSettings["RptPwd"];

                bReportPage obj = new bReportPage();



                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables, crSubTables;

                string SourcePath = new dcommon().Getconfigdata("ReportsLocation");

                DataTable dtDtls = new DataTable();
                dtDtls = obj.GetQueryResult(libReportPage.ReportQuery);
                report.Load(SourcePath + libReportPage.ReportName);
                //report.Load("D:\\Dilranjan Backup\\DILRANJAN PC BACKUP\\D.K. Singh\\Source\\BISTools\\BISTools\\DocReports\\" + libReportPage.ReportName);
                report.SetDataSource(dtDtls);

                crConnectionInfo.ServerName = ServerName;
                crConnectionInfo.DatabaseName = DatabaseName;
                crConnectionInfo.UserID = UserId;
                crConnectionInfo.Password = Password;

                CrTables = report.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
                {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }
                for (int i = 0; i < report.Subreports.Count; i++)
                {
                    crSubTables = report.Subreports[i].Database.Tables;
                    foreach (CrystalDecisions.CrystalReports.Engine.Table CrSubTable in crSubTables)
                    {
                        crtableLogoninfo = CrSubTable.LogOnInfo;
                        crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                        CrSubTable.ApplyLogOnInfo(crtableLogoninfo);
                    }
                }
                report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;

                //**Pass Parameter to report
                DataTable dtparam = libReportPage.ReportParamenter;


                foreach (DataRow Ditem in dtparam.Rows)
                {
                    if (Ditem["Parameter"].ToString().Contains("Date") == true)
                    {

                        report.SetParameterValue(Ditem["Parameter"].ToString(), (Ditem["Value"].ToString()));
                    }
                    else
                    {

                        report.SetParameterValue(Ditem["Parameter"].ToString(), Ditem["Value"].ToString());

                    }

                }

                return report;

            }

            catch (Exception Ex)
            {
                report.Dispose();
                return Ex.ToString();

            }
        }
    }
}

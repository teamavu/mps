﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MPS.Library;
using MPS.BLayer;
using MPS.DLayer;
using System.Data;
using System.Data.SqlClient;
using CustomControls;

namespace MPS
{
    /// <summary>
    /// Interaction logic for ReportPage.xaml
    /// </summary>
    public partial class ReportPage : Page
    {
        libReportPage objRP = new libReportPage();
        bcommon objcom = new bcommon();
        string RepId = "",PrmFlag="";
        public ReportPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           try
           {
              

              bReportPage obj = new bReportPage();
               DataTable dtlist = obj.GetReportList();
               CollectionViewSource cvs = (CollectionViewSource)FindResource("cvs");
               cvs.Source = dtlist;

               DFDReportList.DataContext = cvs;
             
             
               DataTable dt = new DataTable();
               dt.Columns.Add("Report_Display_Name");
               dt.Columns.Add("Report_Name");
               dt.Columns.Add("Source_Query");
               libReportPage.ReportSourceData = dt;

           }
            catch(Exception ex)
           {
                txtmessage.Text=ex.ToString();
                return;
            }


        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }
        public void LoadGrid()
        {
            string str;

           

          
        }

        

       
       

       

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            txtmessage.Text = "";
            try
            {
             
                if (DFDReportFilterValue.SelectedItem != null)
                {
                    DataRowView drv = DFDReportFilterValue.SelectedItem as DataRowView;
                    drv.Row.Delete();

                   

                }

            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            txtFromDate.Text = "";
            txtToDate.Text = "";
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UIServices.SetBusyState();
                DataRowView drv = DFDReportParameter.SelectedItem as DataRowView;
                    DataRow drq = drv.Row;
                    if (drq.ItemArray[ReportFilter.DataType].ToString() != "DATE")
                    {
                    txtmessage.Text = "First Select Date Parameter";
                    return;
                    }


                DataTable dtItem = objRP.FilterValue;

                DataRow dr;
                if (txtFromDate.Text != "")
                {

                    int index = 0;
                    int currentSeq = 0;
                    for (int i = 0; i < dtItem.Rows.Count; i++)
                    {
                        if (dtItem.Rows[i]["ColumnName"].ToString() == drq.ItemArray[ReportFilter.ColumnName].ToString() && dtItem.Rows[i]["DateType"].ToString() == "From")
                        {
                            index = 1;
                            currentSeq = i;
                        }
                    }
                    if (index == 0)
                    {
                        dr = dtItem.NewRow();

                    }
                    else
                    {
                        dr = dtItem.Rows[currentSeq];
                    }


                    dr["ID"] = 0;
                    dr["prm_value"] = txtFromDate.Text;
                    dr["ColumnName"] = drq.ItemArray[ReportFilter.ColumnName].ToString();
                    dr["prm_name"] = drq.ItemArray[ReportFilter.ParameterName].ToString();
                    dr["DataType"] = drq.ItemArray[ReportFilter.DataType].ToString();
                    dr["DateType"] = "From";
                    dr["DateInputType"] = drq.ItemArray[ReportFilter.DateInputType].ToString();
                    dr["Operation"] = drq.ItemArray[ReportFilter.Operator].ToString();
                    if (index == 0)
                    {

                        dtItem.Rows.Add(dr);
                    }
                }
                if (txtToDate.Text != "")
                {

                    int index = 0;
                    int currentSeq = 0;
                    for (int i = 0; i < dtItem.Rows.Count; i++)
                    {
                        if (dtItem.Rows[i]["ColumnName"].ToString() == drq.ItemArray[ReportFilter.ColumnName].ToString() && dtItem.Rows[i]["DateType"].ToString() == "To")
                        {
                            index = 1;
                            currentSeq = i;
                        }
                    }
                    if (index == 0)
                    {
                        dr = dtItem.NewRow();

                    }
                    else
                    {
                        dr = dtItem.Rows[currentSeq];
                    }


                    dr["ID"] = 0;
                    dr["prm_value"] = txtToDate.Text;
                    dr["ColumnName"] = drq.ItemArray[ReportFilter.ColumnName].ToString();
                    dr["prm_name"] = drq.ItemArray[ReportFilter.ParameterName].ToString();
                    dr["DataType"] = drq.ItemArray[ReportFilter.DataType].ToString();
                    dr["DateType"] = "To";
                    dr["DateInputType"] = drq.ItemArray[ReportFilter.DateInputType].ToString();
                    dr["Operation"] = drq.ItemArray[ReportFilter.Operator].ToString();
                    if (index == 0)
                    {

                        dtItem.Rows.Add(dr);
                    }
                }

                DFDReportFilterValue.DataContext = dtItem;


                //add parameter values 

                if (PrmFlag == "1" &&(txtFromDate.Text!="" || txtToDate.Text!=""))
                {
                    DataTable dt = libReportPage.ReportParamenter;
                    DataView dv = new DataView(dt, "ParameterName='" + drq.ItemArray[ReportFilter.ColumnName].ToString() + "'", "", DataViewRowState.CurrentRows);
                    if (dv.Count == 1)
                    {

                    }
                    if (dv.Count == 2)
                    {
                        dv[0]["Value"] = Convert.ToDateTime(txtFromDate.Text);

                        dv[1]["Value"] = ERBFunctions.IsDate(txtToDate.Text) ? Convert.ToDateTime(txtToDate.Text) : Convert.ToDateTime(txtFromDate.Text);
                    }

                }
                txtToDate.Text = "";
                txtFromDate.Text = "";
                  

            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }

        }

        protected Boolean ChkMandatoryField()
        {
            int maindatory = 5, org = 5;
            string mfield = "", orgfield = "";
            string pmName = "", ColmNm = "", Datatype = "", Operation = "", datainputtype = "";
            //DataRowView drv = DFDReportParameter.SelectedItem as DataRowView;
            //DataRow dr = drv.Row;
            try
            {
                DataTable dtRP = objcom.GetGridTable(DFDReportParameter);

                foreach (DataRow dr in dtRP.Rows)
                {

                    if (dr[ReportFilter.rsp_MandatoryFlag].ToString() == "1")
                    {
                        mfield = dr[ReportFilter.ParameterName].ToString();

                        maindatory = 0;
                        foreach (DataRow Ditem in objRP.FilterValue.Rows)
                        {
                            if (dr[ReportFilter.ParameterName].ToString() == Ditem["prm_name"].ToString())
                            {
                                maindatory = 1;
                            }
                        }

                    }
                    if (dr[ReportFilter.rsp_OrgFlag].ToString() == "1")
                    {
                        orgfield = dr[ReportFilter.ParameterName].ToString();

                        ColmNm = dr[ReportFilter.ColumnName].ToString();
                        pmName = dr[ReportFilter.ParameterName].ToString();
                        Datatype = dr[ReportFilter.DataType].ToString();
                        datainputtype = dr[ReportFilter.DateInputType].ToString();
                        Operation = dr[ReportFilter.Operator].ToString();
                        org = 0;
                        foreach (DataRow Ditem in objRP.FilterValue.Rows)
                        {
                            if (dr[ReportFilter.ParameterName].ToString() == Ditem["prm_name"].ToString())
                            {
                                org = 1;
                            }
                        }
                    }
                }
                if (maindatory == 0)
                {
                    txtmessage.Text = "First Select at list One Value for " + mfield;
                    return false;
                }
                if (org == 0)
                {
                    DataTable dtItem = objRP.FilterValue;

                    DataRow drfv;



                    drfv = dtItem.NewRow();



                    drfv["ID"] = CurrentSession.OrgID;
                    drfv["prm_name"] = pmName;
                    drfv["ColumnName"] = ColmNm;
                    drfv["prm_value"] = CurrentSession.OrgName;
                    drfv["DataType"] = Datatype;


                    dtItem.Rows.Add(drfv);


                }

                DFDReportFilterValue.DataContext = objRP.FilterValue;

                return true;
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return false;
            }
        }

        private Boolean PrintReport()
        {
            try
            {
                string ReportSource = "", str = "", ReportFileName = "", ReportFileDisplayName = "", PrintView = "";
                bReportPage obj = new bReportPage();
                DataTable dtRPList = obj.GetReportDetials(RepId);
                if (dtRPList.Rows.Count > 0)
                {
                    str = dtRPList.Rows[0]["Rep_ReportSourceSQL"].ToString();
                    ReportFileName = dtRPList.Rows[0]["Rep_ReportFileName"].ToString();
                    ReportSource = dtRPList.Rows[0]["Rep_ReportSource"].ToString();
                    ReportFileDisplayName = dtRPList.Rows[0]["Rep_DisplayName"].ToString();
                    PrintView = dtRPList.Rows[0]["up_poprn"].ToString();
                }
                DataTable dtFilter = objRP.FilterValue;

                DataTable dtColumn = new DataTable();
                dtColumn.Columns.Add("ColumnName");
                dtColumn.Columns.Add("DataType");
                dtColumn.Columns.Add("DateType");
                dtColumn.Columns.Add("DateInputType");
                dtColumn.Columns.Add("Operation");
                for (int j = 0; j < dtFilter.Rows.Count; j++)
                {
                    DataRow dr;
                    int index = 0;
                    int currentSeq = 0;
                    for (int i = 0; i < dtColumn.Rows.Count; i++)
                    {
                        if (dtFilter.Rows[j]["ColumnName"].ToString() == dtColumn.Rows[i]["ColumnName"].ToString())
                        {
                            index = 1;
                            currentSeq = i;
                        }
                    }
                    if (index == 0)
                    {
                        dr = dtColumn.NewRow();

                    }
                    else
                    {
                        dr = dtColumn.Rows[currentSeq];
                    }

                    dr["ColumnName"] = dtFilter.Rows[j]["columnName"].ToString();
                    dr["DataType"] = dtFilter.Rows[j]["DataType"].ToString();
                    dr["DateType"] = dtFilter.Rows[j]["DateType"].ToString();
                    dr["DateInputType"] = dtFilter.Rows[j]["DateInputType"].ToString();
                    dr["Operation"] = dtFilter.Rows[j]["Operation"].ToString();
                    if (index == 0)
                    {
                        dtColumn.Rows.Add(dr);
                    }
                }
                string wstr = "", temp = "", temp1 = "";
                for (int i = 0; i < dtColumn.Rows.Count; i++)
                {
                    if (dtColumn.Rows[i]["DataType"].ToString() == "NUMBER" || dtColumn.Rows[i]["DataType"].ToString() == "STRING")
                    {
                        if (ReportSource.ToUpper() == "VIEW")
                        {
                            if (temp == "")
                            {
                                wstr += dtColumn.Rows[i]["ColumnName"].ToString() + " in ( ";
                                temp = "00";
                            }
                            else
                            {
                                wstr += " and " + dtColumn.Rows[i]["ColumnName"].ToString() + " in ( ";
                            }
                        }
                        else if (ReportSource.ToUpper() == "SP" && dtColumn.Rows[i]["DateInputType"].ToString() == "MULTIPLE")
                        {
                            if (temp == "")
                            {
                                wstr += dtColumn.Rows[i]["ColumnName"].ToString() + " in ( ";
                                temp = "00";
                            }
                            else
                            {
                                wstr += " , " + dtColumn.Rows[i]["ColumnName"].ToString() + " in ( ";
                            }
                        }
                        else
                        {
                            if (temp == "")
                            {
                                wstr += dtColumn.Rows[i]["ColumnName"].ToString() + " = ";
                                temp = "00";
                            }
                            else
                            {
                                wstr += " , " + dtColumn.Rows[i]["ColumnName"].ToString() + " = ";
                            }
                        }
                        temp1 = "";
                        foreach (DataRow Ditem in dtFilter.Rows)
                        {


                            if (Ditem["ColumnName"].ToString() == dtColumn.Rows[i]["ColumnName"].ToString())
                            {
                                if (Ditem["DataType"].ToString() == "NUMBER")
                                {
                                    if (temp1 == "")
                                    {
                                        wstr += Ditem["ID"].ToString();
                                        temp1 = "00";
                                    }
                                    else
                                    {
                                        wstr += " , " + Ditem["ID"].ToString();
                                    }
                                }
                                else if (Ditem["DataType"].ToString() == "STRING")
                                {
                                    if (temp1 == "")
                                    {
                                        wstr += " '" + Ditem["prm_value"].ToString() + "' ";
                                        temp1 = "00";
                                    }
                                    else
                                    {
                                        wstr += " , '" + Ditem["prm_value"].ToString() + "' ";
                                    }
                                }

                            }
                        }
                        if (ReportSource.ToUpper() == "VIEW" || (ReportSource.ToUpper() == "SP" && dtColumn.Rows[i]["DateInputType"].ToString() == "MULTIPLE"))
                        {
                            wstr += " ) ";
                        }
                    }
                }


                // Condition for Date Type

                string dstr = "", temp2 = "";
                if (ReportSource.ToUpper() == "VIEW")
                {
                    for (int i = 0; i < dtColumn.Rows.Count; i++)
                    {
                        if (dtColumn.Rows[i]["DataType"].ToString() == "DATE")
                        {
                            if (dtColumn.Rows[i]["DateInputType"].ToString() == "DUAL")
                            {

                                temp = "00";

                                temp2 = "";
                                foreach (DataRow Ditem in dtFilter.Rows)
                                {


                                    if (Ditem["ColumnName"].ToString() == dtColumn.Rows[i]["ColumnName"].ToString())
                                    {

                                        if (Ditem["DateType"].ToString() == "From")
                                        {
                                            DateTime date;
                                            date = Convert.ToDateTime(Ditem["prm_value"].ToString());
                                            long fromdt = Int64.Parse(date.ToString("yyyyMMdd"));
                                            if (temp2 != "")
                                            {
                                                dstr += " and  Convert(varchar," + Ditem["ColumnName"].ToString() + ",112) >= " + fromdt;
                                            }
                                            else if (temp2 == "")
                                            {
                                                dstr += "   Convert(varchar," + Ditem["ColumnName"].ToString() + ",112) >= " + fromdt;
                                                temp2 = "fdfdf";
                                            }
                                        }

                                        else if (Ditem["DateType"].ToString() == "To")
                                        {
                                            DateTime date1;
                                            date1 = Convert.ToDateTime(Ditem["prm_value"].ToString());
                                            long fromdt1 = Int64.Parse(date1.ToString("yyyyMMdd"));
                                            if (temp2 != "")
                                            {
                                                dstr += " and  Convert(varchar," + Ditem["ColumnName"].ToString() + ",112) <= " + fromdt1;
                                            }
                                            else if (temp2 == "")
                                            {
                                                dstr += "   Convert(varchar," + Ditem["ColumnName"].ToString() + ",112) <= " + fromdt1;
                                                temp2 = "fdfdf";
                                            }
                                        }



                                    }
                                }
                            }
                            if (dtColumn.Rows[i]["DateInputType"].ToString() == "SINGLE")
                            {

                                temp = "00";

                                temp2 = "";
                                foreach (DataRow Ditem in dtFilter.Rows)
                                {


                                    if (Ditem["ColumnName"].ToString() == dtColumn.Rows[i]["ColumnName"].ToString())
                                    {
                                        DateTime date1;
                                        date1 = Convert.ToDateTime(Ditem["prm_value"].ToString());
                                        long fromdt1 = Int64.Parse(date1.ToString("yyyyMMdd"));

                                        dstr += "  Convert(varchar," + Ditem["ColumnName"].ToString() + ",112)" + " " + Ditem["Operation"].ToString() + " '" + fromdt1 + " '";

                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    GetPeriodDates();

                    DateTime date1;
                    if (hdnFromDate.Text == "")
                    {
                        date1 = Convert.ToDateTime(CurrentSession.PeriodFrom);
                    }
                    else
                    {
                        date1 = Convert.ToDateTime(hdnFromDate.Text);
                    }

                    long fromdt1 = Int64.Parse(date1.ToString("yyyyMMdd"));

                    DateTime date2;
                    if (hdnToDate.Text == "")
                    {
                        date2 = Convert.ToDateTime(CurrentSession.PeriodTo);
                    }
                    else
                    {
                        date2 = Convert.ToDateTime(hdnToDate.Text);
                    }
                    long fromdt2 = Int64.Parse(date2.ToString("yyyyMMdd"));

                    dstr = "@TransFromDate='" + fromdt1 + "' , @TransToDate='" + fromdt2 + "'";

                }

                if (ReportSource.ToUpper() == "VIEW")
                {
                    if (dstr == "" && wstr == "")
                    {
                        wstr = "Select * from (" + str + ") ReportList ";
                    }


                    else if (dstr == "")
                    {
                        wstr = "Select * from (" + str + ") ReportList Where " + wstr;
                    }
                    else if (wstr == "")
                    {
                        wstr = "Select * from (" + str + ") ReportList Where " + dstr;
                    }
                    else
                        wstr = "Select * from (" + str + ") ReportList Where " + wstr + " and " + dstr;
                    //   lblMessage.Text = wstr;
                }
                else
                {
                    if (dstr == "" && wstr == "")
                    {
                        wstr = str;
                    }


                    else if (dstr == "")
                    {
                        wstr = str + " " + wstr;
                    }
                    else if (wstr == "")
                    {
                        wstr = str + " " + dstr;
                    }
                    else
                        wstr = str + " " + wstr + "," + dstr;
                }

                DataTable dtSource = libReportPage.ReportSourceData;
                DataRow drs;
                drs = dtSource.NewRow();
                drs["Report_Name"] = ReportFileName;
                drs["Source_Query"] = wstr;
                drs["Report_Display_Name"] = ReportFileDisplayName;
                dtSource.Rows.Add(drs);

                libReportPage.ReportQuery = wstr;
                libReportPage.ReportName = ReportFileName;
                libReportPage.ReportPrintorNot = PrintView;
                DataTable dReprm = libReportPage.ReportParamenter;

               

                return true;
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return false;
            }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            try
            {

                if (ChkMandatoryField() != true) { return; }

                UIServices.SetBusyState();
                if (PrintReport() != true) return;
                Report NewRep = new Report();
                NewRep.Show();
               
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }
        }


        protected void GetPeriodDates()
        {
            DataTable dt = objRP.FilterValue;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ColumnName"].ToString() == "@TransFromDate")
                {
                    hdnFromDate.Text = dt.Rows[i]["prm_value"].ToString();
                }
                if (dt.Rows[i]["ColumnName"].ToString() == "@TransToDate")
                {
                    hdnToDate.Text = dt.Rows[i]["prm_value"].ToString();
                }
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.DFDReportList.Focus();
            ShowQuery.IsOpen = false;
        }
        
        private void btnBank_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            if (ModifierKeys.Control == Keyboard.Modifiers && e.Key == Key.Q)
            {
                DFDShowQuery.DataContext = libReportPage.ReportSourceData;
                ShowQuery.IsOpen = true;

            }
        }

        private void DFDShowQuery_CopyingRowClipboardContent(object sender, DataGridRowClipboardEventArgs e)
        {
            //var currentCell = e.ClipboardRowContent[DFDShowQuery.CurrentCell.Column.DisplayIndex];
            //e.ClipboardRowContent.Clear();
            //e.ClipboardRowContent.Add(currentCell);
        }

        private void DFDReportList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtmessage.Text = "";
            try
            {
                UIServices.SetBusyState();
                bReportPage obj = new bReportPage();
                if (DFDReportList.SelectedItem != null)
                {


                    DataRowView drv = DFDReportList.SelectedItem as DataRowView;
                    DataRow drl = drv.Row;
                    RepId = drl.ItemArray[1].ToString();
                    DataTable dtlist = obj.GetParameterList(drl["rep_id"].ToString());
                    DFDReportParameter.DataContext = dtlist;


                    DFDReportParameterValue.DataContext = "";
                    DFDReportFilterValue.DataContext = "";

                    BorderDetailsparamdt.Visibility = Visibility.Collapsed;
                    DFDReportParameterValue.Visibility = Visibility.Visible;

                    DataTable dtprmvalue = new DataTable();
                    dtprmvalue.Columns.Add("SeqNo");
                    dtprmvalue.Columns.Add("prm_name");
                    dtprmvalue.Columns.Add("prm_value");
                    dtprmvalue.Columns.Add("ID");
                    dtprmvalue.Columns.Add("ColumnName");
                    dtprmvalue.Columns.Add("DataType");
                    dtprmvalue.Columns.Add("DateInputType");
                    dtprmvalue.Columns.Add("DateType");
                    dtprmvalue.Columns.Add("Operation");


                    objRP.FilterValue = dtprmvalue;


                    DataTable dtt = new DataTable();
                    dtt.Columns.Add("Parameter");
                    dtt.Columns.Add("ParameterName");
                    dtt.Columns.Add("Value");

                    libReportPage.ReportParamenter = dtt;

                    DataTable dtprm = objcom.GetGridTable(DFDReportParameter);

                    foreach (DataRow Item in dtprm.Rows)
                    {
                        PrmFlag = Item["rsp_ParFlag"].ToString();

                        if (Item["rsp_ParFlag"].ToString() == "1")
                        {


                            if (Item["rsp_ParName1"].ToString() != "0")
                            {
                                DataRow dr;
                                int index = 0, currseq = 0;
                                for (int i = 0; i < dtt.Rows.Count; i++)
                                {
                                    if (dtt.Rows[i]["Parameter"].ToString() == Item["rsp_ParName1"].ToString() && dtt.Rows[i]["ParameterName"].ToString() == Item["rsp_SourceColumnToFilter"].ToString())
                                    {
                                        index = 1;
                                        currseq = i;
                                    }
                                }
                                if (index == 0)
                                {
                                    dr = dtt.NewRow();
                                }
                                else
                                {
                                    dr = dtt.Rows[currseq];
                                }
                                dr["Parameter"] = Item["rsp_ParName1"].ToString();
                                dr["ParameterName"] = Item["rsp_SourceColumnToFilter"].ToString();
                                if (index == 0)
                                {
                                    dtt.Rows.Add(dr);
                                }
                            }
                            if (Item["rsp_ParName2"].ToString() != "0")
                            {
                                DataRow dr;
                                int index = 0, currseq = 0;
                                for (int i = 0; i < dtt.Rows.Count; i++)
                                {
                                    if (dtt.Rows[i]["Parameter"].ToString() == Item["rsp_ParName2"].ToString() && dtt.Rows[i]["ParameterName"].ToString() == Item["rsp_SourceColumnToFilter"].ToString())
                                    {
                                        index = 1;
                                        currseq = i;
                                    }
                                }
                                if (index == 0)
                                {
                                    dr = dtt.NewRow();
                                }
                                else
                                {
                                    dr = dtt.Rows[currseq];
                                }
                                dr["Parameter"] = Item["rsp_ParName2"].ToString();
                                dr["ParameterName"] = Item["rsp_SourceColumnToFilter"].ToString();
                                if (index == 0)
                                {
                                    dtt.Rows.Add(dr);
                                }
                            }


                        }

                    }
                }
            }

            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }
        }

        private void DFDReportParameter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtmessage.Text = "";
            try
            {
                UIServices.SetBusyState();
                bReportPage obj = new bReportPage();
                if (DFDReportParameter.SelectedItem != null)
                {
                    DataRowView drv = DFDReportParameter.SelectedItem as DataRowView;
                    DataRow dr = drv.Row;

                    PrmFlag = dr["rsp_ParFlag"].ToString();
                    if (dr.ItemArray[ReportFilter.DataType].ToString() != "DATE")
                    {
                        string str = dr.ItemArray[ReportFilter.LOvSQlQuery].ToString();
                        if (dr.ItemArray[ReportFilter.rsp_OrgFilter].ToString() != "")
                        {
                            if (str.Contains("where") == true)
                            {
                                str = str + " and  " + dr.ItemArray[ReportFilter.rsp_OrgFilter].ToString() + "=" + CurrentSession.OrgID;
                            }
                            else
                            {
                                str = str + " Where " + dr.ItemArray[ReportFilter.rsp_OrgFilter].ToString() + "=" + CurrentSession.OrgID;
                            }
                        }

                        DataTable dtlist = obj.GetParameterValues(str);
                        DFDReportParameterValue.DataContext = dtlist;

                        BorderDetailsparamdt.Visibility = Visibility.Collapsed;
                        DFDReportParameterValue.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        BorderDetailsparamdt.Visibility = Visibility.Visible;
                        DFDReportParameterValue.Visibility = Visibility.Collapsed;

                        if (dr.ItemArray[ReportFilter.DateInputType].ToString() != "DUAL")
                        {
                            txtToDate.Visibility = Visibility.Collapsed;
                            tolbl.Visibility = Visibility.Collapsed;
                            toll.Visibility = Visibility.Collapsed;
                            Frmlbl.Text = "Date";
                            txtFromDate.Text = "";
                            txtToDate.Text = "";

                        }
                        else
                        {
                            Frmlbl.Text = "From Date";
                            txtToDate.Visibility = Visibility.Visible;
                            tolbl.Visibility = Visibility.Visible;
                            toll.Visibility = Visibility.Visible;
                            txtFromDate.Text = "";
                            txtToDate.Text = "";
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }
        }

       
        private void DFDReportParameterValue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtmessage.Text = "";
            try
            {
                UIServices.SetBusyState();
                DataTable dtprmvalue = objRP.FilterValue;

                DataRowView drp = DFDReportParameter.SelectedItem as DataRowView;
                DataRow dr = drp.Row;


                DataRowView drpv = DFDReportParameterValue.SelectedItem as DataRowView;
                DataRow drv = drpv.Row;


                DataRow dr1;
                int index = 0, currSeq = 0;
                for (int i = 0; i < dtprmvalue.Rows.Count; i++)
                {
                    if (dr.ItemArray[ReportFilter.DataType].ToString().Trim().ToUpper() == "NUMBER")
                    {
                        if (dtprmvalue.Rows[i]["ColumnName"].ToString() == dr.ItemArray[ReportFilter.ColumnName].ToString() && dtprmvalue.Rows[i]["ID"].ToString() == drv["ID"].ToString())
                        {
                            index = 1;
                            currSeq = i;
                        }
                    }
                    else
                    {
                        if (dtprmvalue.Rows[i]["ColumnName"].ToString() == dr.ItemArray[ReportFilter.ColumnName].ToString() && dtprmvalue.Rows[i]["prm_value"].ToString() == drv["NAme"].ToString())
                        {
                            index = 1;
                            currSeq = i;
                        }
                    }
                }
                if (index == 0)
                {


                    dr1 = dtprmvalue.NewRow();
                }
                else
                {
                    dr1 = dtprmvalue.Rows[currSeq];
                }
                dr1["prm_name"] = dr.ItemArray[ReportFilter.ParameterName].ToString();
                dr1["prm_value"] = drv["NAme"].ToString();

                dr1["ID"] = drv["ID"].ToString();
                dr1["ColumnName"] = dr.ItemArray[ReportFilter.ColumnName].ToString();
                dr1["DataType"] = dr.ItemArray[ReportFilter.DataType].ToString();
                dr1["DateInputType"] = dr.ItemArray[ReportFilter.DateInputType].ToString();
                dr1["Operation"] = dr.ItemArray[ReportFilter.Operator].ToString();
                if (index == 0)
                {
                    dtprmvalue.Rows.Add(dr1);
                }

                DFDReportFilterValue.DataContext = dtprmvalue;

                //Add Value in Parameter Value
                if (PrmFlag == "1")
                {
                    int a = 0;

                    DataTable dt = libReportPage.ReportParamenter;
                    DataView dv = new DataView(dt, "ParameterName='" + dr.ItemArray[ReportFilter.ColumnName].ToString() + "'", "", DataViewRowState.CurrentRows);
                    if (dv.Count == 1)
                    {
                        if (a > 0)
                        {
                            dv[0]["Value"] = dv[0]["Value"] + ",  " + drv["NAme"].ToString();
                        }
                        else
                        {
                            dv[0]["Value"] = drv["NAme"].ToString();
                        }
                    }
                    if (dv.Count == 2)
                    {

                    }
                    a++;
                }

                //end
            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }
        }

        private void btnPDF_Click(object sender, RoutedEventArgs e)
        {
            txtmessage.Text = "";
            try
            {

                if (ChkMandatoryField() != true) { return; }

                UIServices.SetBusyState();
                if (PrintReport() != true) return;
                ExportPDF NewRep = new ExportPDF();
                NewRep.Show();
                NewRep.Hide();

            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.ToString();
                return;
            }
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

       
    }
}

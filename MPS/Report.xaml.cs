﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using SAPBusinessObjects.WPF.Viewer;
using CrystalDecisions.ReportSource;
using System.Windows.Controls.Primitives;
using SAPBusinessObjects.WPF.ViewerShared;
using System.IO;
using MPS.Library;
using MPS.BLayer;
using MPS.DLayer;
using System.Data;
using System.Data.SqlClient;
using CustomControls;

namespace MPS
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : Window
    {
        public Report()
        {
            InitializeComponent();
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try{
           
            }
            catch (Exception ex)
            {
                return;
            }
        }

        //private void crystalReportsViewer1_Loaded(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        bcommon bcom = new bcommon();

        //        string ServerName = dcommon.ServerName;//bcom. //"BIZSERVER";// System.Configuration.ConfigurationManager.AppSettings["RptServer"];
        //        string DatabaseName = "BizERP";// System.Configuration.ConfigurationManager.AppSettings["RptDatabase"];
        //        string UserId = "sa";// System.Configuration.ConfigurationManager.AppSettings["RptUser"];
        //        string Password = "sql2008";// System.Configuration.ConfigurationManager.AppSettings["RptPwd"];

        //        bReportPage obj = new bReportPage();
        //        ReportDocument report = new ReportDocument();


        //        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        //        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        //        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        //        Tables CrTables, crSubTables;

        //        string SourcePath = new dcommon().Getconfigdata("ReportsLocation");

        //        DataTable dtDtls = new DataTable();
        //        dtDtls = obj.GetQueryResult(libReportPage.ReportQuery);
        //        report.Load(SourcePath + libReportPage.ReportName);
        //        //report.Load("D:\\Dilranjan Backup\\DILRANJAN PC BACKUP\\D.K. Singh\\Source\\BISTools\\BISTools\\DocReports\\" + libReportPage.ReportName);
        //        report.SetDataSource(dtDtls);

        //        crConnectionInfo.ServerName = ServerName;
        //        crConnectionInfo.DatabaseName = DatabaseName;
        //        crConnectionInfo.UserID = UserId;
        //        crConnectionInfo.Password = Password;

        //        CrTables = report.Database.Tables;
        //        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        //        {
        //            crtableLogoninfo = CrTable.LogOnInfo;
        //            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
        //            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        //        }
        //        for (int i = 0; i < report.Subreports.Count; i++)
        //        {
        //            crSubTables = report.Subreports[i].Database.Tables;
        //            foreach (CrystalDecisions.CrystalReports.Engine.Table CrSubTable in crSubTables)
        //            {
        //                crtableLogoninfo = CrSubTable.LogOnInfo;
        //                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
        //                CrSubTable.ApplyLogOnInfo(crtableLogoninfo);
        //            }
        //        }
        //       //report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
        //        report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
             

        //        //**commented******
        //        DataTable dtparam = libReportPage.ReportParamenter;
        //        foreach (DataRow Ditem in dtparam.Rows)
        //        {
        //            if (Ditem["Parameter"].ToString().Contains("Date") == true)
        //            {
        //                report.SetParameterValue(Ditem["Parameter"].ToString(), Ditem["Value"].ToString());
        //            }
        //            else
        //            {
        //                //if (Ditem["Value"].ToString() == "")
        //                //{
        //                //    cryRpt.SetParameterValue(Ditem["Parameter"].ToString(), "");
        //                //}
        //                //else
        //                //{
        //                report.SetParameterValue(Ditem["Parameter"].ToString(), Ditem["Value"].ToString());
        //                //}
        //            }
        //        }
        //        ////**commented******

        //        //report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;

        //        crystalReportsViewer1.ViewerCore.ReportSource = report;
        //        //crystalReportsViewer1.ShowToggleSidePanelButton = false;
        //        if (libReportPage.ReportPrintorNot.ToUpper().Trim() == "PRINT")
        //        {
        //            crystalReportsViewer1.ShowPrintButton = true;
        //            crystalReportsViewer1.ShowExportButton = true;
        //        }
        //        else
        //        {

        //            crystalReportsViewer1.ShowPrintButton = false;
        //            crystalReportsViewer1.ShowExportButton = false;
        //        }
                               
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message.ToUpper().Contains("LOAD REPORT FAILED"))
        //        {
        //            MessageBox.Show(ex.Message, "E`xception!", MessageBoxButton.OK, MessageBoxImage.Error);
        //            return;
        //        }                
        //    }
        //}



        private void crystalReportsViewer1_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                bcommon bcom = new bcommon();

                string ServerName = dcommon.ServerName;//bcom. //"BIZSERVER";// System.Configuration.ConfigurationManager.AppSettings["RptServer"];
                string DatabaseName = "BizERP";// System.Configuration.ConfigurationManager.AppSettings["RptDatabase"];
                string UserId = "ba";// System.Configuration.ConfigurationManager.AppSettings["RptUser"];
                string Password = "sql2008";// System.Configuration.ConfigurationManager.AppSettings["RptPwd"];

                bReportPage obj = new bReportPage();
                ReportDocument report = new ReportDocument();


                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables, crSubTables;

                string SourcePath = new dcommon().Getconfigdata("ReportsLocation");

                DataTable dtDtls = new DataTable();
                dtDtls = obj.GetQueryResult(libReportPage.ReportQuery);
                report.Load(SourcePath + libReportPage.ReportName);
                //report.Load("D:\\Dilranjan Backup\\DILRANJAN PC BACKUP\\D.K. Singh\\Source\\BISTools\\BISTools\\DocReports\\" + libReportPage.ReportName);
                report.SetDataSource(dtDtls);

                crConnectionInfo.ServerName = ServerName;
                crConnectionInfo.DatabaseName = DatabaseName;
                crConnectionInfo.UserID = UserId;
                crConnectionInfo.Password = Password;

                CrTables = report.Database.Tables;
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
                {
                    crtableLogoninfo = CrTable.LogOnInfo;
                    crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crtableLogoninfo);
                }
                for (int i = 0; i < report.Subreports.Count; i++)
                {
                    crSubTables = report.Subreports[i].Database.Tables;
                    foreach (CrystalDecisions.CrystalReports.Engine.Table CrSubTable in crSubTables)
                    {
                        crtableLogoninfo = CrSubTable.LogOnInfo;
                        crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                        CrSubTable.ApplyLogOnInfo(crtableLogoninfo);
                    }
                }
                //report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                crystalReportsViewer1.ViewerCore.ReportSource = report;

                //**commented******
                DataTable dtparam = libReportPage.ReportParamenter;
                foreach (DataRow Ditem in dtparam.Rows)
                {
                    if (Ditem["Parameter"].ToString().Contains("Date") == true)
                    {
                        report.SetParameterValue(Ditem["Parameter"].ToString(), Ditem["Value"].ToString());
                    }
                    else
                    {
                        //if (Ditem["Value"].ToString() == "")
                        //{
                        //    cryRpt.SetParameterValue(Ditem["Parameter"].ToString(), "");
                        //}
                        //else
                        //{
                        report.SetParameterValue(Ditem["Parameter"].ToString(), Ditem["Value"].ToString());
                        //}
                    }
                }
                ////**commented******

                //report.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;


                //crystalReportsViewer1.ShowToggleSidePanelButton = false;
                if (libReportPage.ReportPrintorNot.ToUpper().Trim() == "PRINT")
                {
                    crystalReportsViewer1.ShowPrintButton = true;
                    crystalReportsViewer1.ShowExportButton = true;
                }
                else
                {

                    crystalReportsViewer1.ShowPrintButton = false;
                    crystalReportsViewer1.ShowExportButton = false;
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.ToUpper().Contains("LOAD REPORT FAILED"))
                {
                    MessageBox.Show(ex.Message, "Exception!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }

    }
}

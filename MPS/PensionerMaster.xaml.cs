﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using MPS.Library;
using MPS.DLayer;
using MPS.BLayer;

namespace MPS
{
    /// <summary>
    /// Interaction logic for PensionerMaster.xaml
    /// </summary>
    public partial class PensionerMaster : Page
    {
        string Mode = string.Empty;
        bcommon bcom = new bcommon();
        bPensionerMaster bclass = new bPensionerMaster();
        bool IsFirstLoad = true;
        public PensionerMaster()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsFirstLoad)
            {
                IsFirstLoad = false;
                Mode = "Load";
                SetPageMode();
                LoadDDL();
            }
        }

        public void EnableDisableButtons()
        {
            if (Mode == "Find")
            {
                BtnFind.IsEnabled = true;
                BtnAddNew.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnSave.IsEnabled = false;
                BtnEdit.IsEnabled = true;
                BtnRefresh.IsEnabled = false;
                BtnDelete.IsEnabled = true;
            }
            else if (Mode == "Load")
            {
                BtnFind.IsEnabled = true;
                BtnAddNew.IsEnabled = true;
                BtnRefresh.IsEnabled = false;
                BtnCancel.IsEnabled = false;
                BtnSave.IsEnabled = false;
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
            }
            else if (Mode == "Add")
            {
                BtnRefresh.IsEnabled = true;
                BtnSave.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnAddNew.IsEnabled = false;
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
                BtnFind.IsEnabled = false;
            }
            else if (Mode == "Edit")
            {
                BtnRefresh.IsEnabled = true;
                BtnSave.IsEnabled = true;
                BtnCancel.IsEnabled = true;
                BtnAddNew.IsEnabled = false;
                BtnEdit.IsEnabled = false;
                BtnDelete.IsEnabled = false;
                BtnFind.IsEnabled = false;
            }
           
        }

        public void LoadDDL()
        {
            bcom.FillLOVDDL(ddlType, "PensionerType", "", "","");
            bcom.FillLOVDDL(ddlPaymentMethod, "PaymentType", "", "", "");
            bcom.FillLOVDDL(ddlToRenewOnMonth, "PensionerMonths", "", "", "");
        }

        public void SetPageMode()
        {
            EnableDisableButtons();
            if (Mode == "Load")
            {
                txtmessage.Text = "";
               
                ClearPageControls();
                ControlGrid.IsEnabled = false;
            }
            else if (Mode == "Add")
            {
                txtmessage.Text = "";
                ControlGrid.IsEnabled = true;
                ClearPageControls();
                txtName.Focus();
          
            }
            if (Mode == "Find")
            {
                txtmessage.Text = "";
                ControlGrid.IsEnabled = false;

            }
            else if (Mode == "Edit")
            {
                txtmessage.Text = "";
                ControlGrid.IsEnabled = true;
            }
        }

        public void Retrieve(int id)
        {
            try
            {
                object obj = bclass.Retrieve(id);
                if(obj is DataSet)
                {
                    DataSet ds = (DataSet)obj;
                    DataTable dt = ds.Tables[0];
                    if(dt.Rows.Count > 0)
                    {
                        txtid.Text = dt.Rows[0]["PMast_Id"].ToString();
                        txtName.Text = dt.Rows[0]["PMast_Name"].ToString();
                        txtAddr1.Text = dt.Rows[0]["PMast_Address1"].ToString();
                        txtAddr2.Text = dt.Rows[0]["PMast_Address2"].ToString();
                        txtAddr3.Text = dt.Rows[0]["PMast_Address3"].ToString();
                        txtAddr4.Text = dt.Rows[0]["PMast_Address4"].ToString();
                        txtPinNo.Text = dt.Rows[0]["PMast_PinNo"].ToString();
                        txtRefNo.Text = dt.Rows[0]["PMast_RefNo"].ToString();
                        ddlType.SelectedValue = dt.Rows[0]["PMast_Type"].ToSafeInteger();
                        txtMonthlyAmt.Text = dt.Rows[0]["PMast_MonthlyAmt"].ToString();
                        txtMoc.Text = dt.Rows[0]["PMast_MOC"].ToString();
                        ddlPaymentMethod.SelectedValue = dt.Rows[0]["PMast_PaymentMethod"].ToSafeInteger();
                        txtAdharNo.Text = dt.Rows[0]["PMast_AdharNo"].ToString();
                        txtVoterId.Text = dt.Rows[0]["PMast_VoterIdNo"].ToString();
                        txtpanno.Text = dt.Rows[0]["PMast_PanNo"].ToString();
                        txtDriverLicNo.Text = dt.Rows[0]["PMast_DriveLicNo"].ToString();
                        txtMobNo.Text = dt.Rows[0]["PMast_MobileNo"].ToString();
                        txtOtherRefNo.Text = dt.Rows[0]["PMast_OtherRefNo"].ToString();
                        txtbankAc.Text = dt.Rows[0]["PMast_BankACNo"].ToString();
                        txtBankBranch.Text= dt.Rows[0]["PMast_BankBranch"].ToString();
                        txtbankIfscCode.Text = dt.Rows[0]["PMast_BankIFSCCode"].ToString();
                        txtRemarks.Text = dt.Rows[0]["PMast_Remark"].ToString();
                        ddlToRenewOnMonth.SelectedValue = dt.Rows[0]["RenewOnMonth"].ToString();
                        txtDate.Text = dt.Rows[0]["LastRenewDate"].ToString();
                        if (dt.Rows[0]["PMast_InActive"].ToSafeInteger() == 1)
                        {
                            ChkInactive.IsChecked = true;
                        }
                        else
                        {
                            ChkInactive.IsChecked = false;
                        }
                        txtInactiveDate.Text = dt.Rows[0]["InActiveDate"].ToString();
                        txtInactiveReason.Text = dt.Rows[0]["PMast_InActiveReason"].ToString();
                        txtBankName.Text= dt.Rows[0]["PMast_Bank"].ToString();
                        txtMembershipNo.Text= dt.Rows[0]["PMast_MembershipNo"].ToString();

                    }
                }
                Mode = "Find";
                SetPageMode();

            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
           
        }

        public void ClearPageControls()
        {
            txtid.Text = "0";
            txtName.Text = string.Empty;
            txtAddr1.Text = string.Empty;
            txtAddr2.Text = string.Empty;
            txtAddr3.Text = string.Empty;
            txtAddr4.Text = string.Empty;
            txtPinNo.Text = string.Empty;
            txtRefNo.Text = string.Empty;
            ddlType.SelectedValue = 0;
            txtMonthlyAmt.Text = string.Empty;
            txtMoc.Text = string.Empty;
            ddlPaymentMethod.SelectedValue = 0;
            txtAdharNo.Text = string.Empty;
            txtVoterId.Text = string.Empty;
            txtpanno.Text = string.Empty;
            txtDriverLicNo.Text = string.Empty;
            txtMobNo.Text = string.Empty;
            txtOtherRefNo.Text = string.Empty;
            txtbankAc.Text = string.Empty;
            txtbankIfscCode.Text = string.Empty;
            ddlToRenewOnMonth.SelectedValue = 0;
            txtDate.Text = string.Empty;
            ChkInactive.IsChecked = false;
            txtInactiveDate.Text = string.Empty;
            txtInactiveReason.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            txtBankBranch.Text = string.Empty;
            txtBankName.Text = string.Empty;
            txtMembershipNo.Text = string.Empty;
        }

        public void SetData(LibPensionerMaster lib)
        {
            lib.PMast_Id = txtid.Text.ToSafeInteger();
            lib.PMast_Name = txtName.Text;
            lib.PMast_Address1 = txtAddr1.Text;
            lib.PMast_Address2 = txtAddr2.Text;
            lib.PMast_Address3=txtAddr3.Text;
            lib.PMast_Address4 = txtAddr4.Text;
            lib.PMast_PinNo = txtPinNo.Text.ToString();
            lib.PMast_RefNo = txtRefNo.Text;
            lib.PMast_Type = ddlType.SelectedValue.ToSafeInteger();
            lib.PMast_MonthlyAmt = txtMonthlyAmt.Text.ToSafeDouble();
            lib.PMast_MOC = txtMoc.Text.ToSafeDouble();
            lib.PMast_PaymentMethod = ddlPaymentMethod.SelectedValue.ToSafeInteger();
            lib.PMast_AdharNo = txtAdharNo.Text;
            lib.PMast_VoterIdNo = txtVoterId.Text;
            lib.PMast_PanNo = txtpanno.Text;
            lib.PMast_DriveLicNo = txtDriverLicNo.Text;
            lib.PMast_MobileNo = txtMobNo.Text;
            lib.PMast_OtherRefNo = txtOtherRefNo.Text;
            lib.PMast_BankACNo = txtbankAc.Text;
            lib.PMast_BankBranch = txtBankBranch.Text;
            lib.PMast_BankIFSCCode = txtbankIfscCode.Text;
            lib.PMast_RenewOnMonth = ddlPaymentMethod.SelectedValue.ToString()=="0" ? string.Empty : ddlToRenewOnMonth.Text;
            lib.PMast_LastRenewOn = txtDate.Text.ToString()==string.Empty ? "" : Convert.ToDateTime(txtDate.Text).ToString("yyyyMMdd");
            if (ChkInactive.IsChecked == true)
            {
                lib.PMast_InActive = 1;
            }
            else
            {
                lib.PMast_InActive = 0;
            }

            lib.PMast_InActiveOn = txtInactiveDate.Text.ToString()==string.Empty ? "" :Convert.ToDateTime(txtInactiveDate.Text).ToString("yyyyMMdd");
            lib.PMast_InActiveReason = txtInactiveReason.Text;
            lib.PMast_Remark=txtRemarks.Text;
            lib.PMast_BankName = txtBankName.Text;
            lib.PMast_MembershipNo = txtMembershipNo.Text;

        }
        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnAddNew_Click(object sender, RoutedEventArgs e)
        {
            Mode = "Add";
            SetPageMode();
            
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Mode = "Edit";
            SetPageMode();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
              
                Mode = "Delete";
                LibPensionerMaster lib = new LibPensionerMaster();
                SetData(lib);
                object obj = bclass.Update(lib, Mode);
                if (obj is DataSet)
                {
                    Mode = "Load";
                    SetPageMode();
                    txtmessage.Text = "Record Deleted Successfully";
                }
            }
            catch(Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
          
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LibPensionerMaster lib = new LibPensionerMaster();
                if (txtName.Text.Trim() == string.Empty)
                {
                    txtmessage.Text = "Enter Name";
                    txtName.Focus();
                    return;
                }
                else if (ddlType.SelectedValue.ToSafeInteger() == 0)
                {
                    txtmessage.Text = "Select Pensioner Type";
                    ddlType.Focus();
                    return;
                }
                else if (txtAdharNo.Text.Trim() != string.Empty)
                {
                    if ((bclass.CheckDuplicateAdharNo(txtid.Text.ToSafeInteger(), txtAdharNo.Text)) != true)
                    {
                        txtmessage.Text = "Duplicate AdharNo Found";
                        txtAdharNo.Focus();
                        return;
                    }
                }
                else if (txtpanno.Text.Trim() != string.Empty)
                {
                    if ((bclass.DuplicatePanNo(txtid.Text.ToSafeInteger(), txtpanno.Text)) != true)
                    {
                        txtmessage.Text = "Duplicate PanNo Found";
                        txtpanno.Focus();
                        return;
                    }
                }
                else if (txtVoterId.Text.Trim() != string.Empty)
                {
                    if ((bclass.DuplicateVoterId(txtid.Text.ToSafeInteger(), txtVoterId.Text)) != true)
                    {
                        txtmessage.Text = "Duplicate Voter Id Found";
                        txtVoterId.Focus();
                        return;
                    }
                }
                else if (txtDriverLicNo.Text.Trim() != string.Empty)
                {
                    if ((bclass.DuplicateDriverLicNo(txtid.Text.ToSafeInteger(), txtDriverLicNo.Text)) != true)
                    {
                        txtmessage.Text = "Duplicate Driving License No Found";
                        txtDriverLicNo.Focus();
                        return;
                    }
                }
                else if (txtMembershipNo.Text.Trim() != string.Empty)
                {
                    if ((bclass.DuplicateMembershipNo(txtid.Text.ToSafeInteger(), txtMembershipNo.Text)) != true)
                    {
                        txtmessage.Text = "Duplicate Membership no Found";
                        txtMembershipNo.Focus();
                        return;
                    }
                }
                else if(ddlType.SelectedValue.ToSafeInteger() > 0 && txtRefNo.Text !=string.Empty)
                {
                    if ((bclass.DuplicateRefNo(txtid.Text.ToSafeInteger(),ddlType.SelectedValue.ToSafeInteger(),txtRefNo.Text)) != true)
                    {
                        txtmessage.Text = "Duplicate Reference No Found For "+ddlType.Text;
                        txtRefNo.Focus();
                        return;
                    }
                }
                SetData(lib);
                object obj = bclass.Update(lib, Mode);
                if (obj is DataSet)
                {

                    Mode = "Find";
                    SetPageMode();
                    txtid.Text = lib.PMast_Id.ToString();
                    Retrieve(txtid.Text.ToSafeInteger());
                    txtmessage.Text = "Record Saved Successfully";
                }
                else
                {
                    txtmessage.Text = obj.ToString();
                }

            }
            catch (Exception ex)
            {
                txtmessage.Text = ex.Message;
            }
           

        }

        private void BtnFind_Click(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            Find.FillFind("Pensionermaster");
            FindPopup.IsOpen = true;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            UIServices.SetBusyState();
            Mode = "Load";
            SetPageMode();
        }

        private void BtnRefresh_Click(object sender, RoutedEventArgs e)
        {
            if (ddlType.SelectedValue.ToSafeInteger() == 0) { bcom.FillLOVDDL(ddlType, "PensionerType", "", "", ""); }
            if (ddlPaymentMethod.SelectedValue.ToSafeInteger() == 0)
            {
                bcom.FillLOVDDL(ddlPaymentMethod, "PaymentType", "", "", "");
            }
            if (ddlToRenewOnMonth.SelectedValue.ToSafeInteger() == 0)
            {
                bcom.FillLOVDDL(ddlToRenewOnMonth, "PensionerMonths", "", "", "");
            }
        }

        private void Find_OnSelectionChanged(object sender, EventArgs e)
        {
            UIServices.SetBusyState();
            try
            {
                FindPopup.IsOpen = false;
                DataRowView Drv = Find.SelectedItem;
                txtid.Text = Convert.ToString(Drv.Row["ID"]);
                Retrieve(txtid.Text.ToSafeInteger());
                
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ddlType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(ddlType.SelectedValue.ToSafeInteger() > 0)
            {
                DataTable dt = bclass.GetPensionerType(ddlType.SelectedValue.ToSafeInteger());
                if (dt.Rows.Count > 0)
                {
                    txtMonthlyAmt.Text = dt.Rows[0]["Pt_MonthlyAmt"].ToString();
                    txtMoc.Text = dt.Rows[0]["Pt_MocAmtQtyly"].ToString();
                }
                else
                {
                    txtMonthlyAmt.Text = string.Empty;
                    txtMoc.Text = string.Empty;
                }
            }
            else
            {
                txtMonthlyAmt.Text = string.Empty;
                txtMoc.Text = string.Empty;
            }
        }

        

        //private void txtPinNo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        //{
        //    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[.][0-9]{0,12}$|^[0-9]*[.]{0,1}[0-9]{0,2}[0-9]{0,2}$");
        //    e.Handled = !regex.IsMatch((sender as TextBox).Text.Insert(((sender as TextBox).SelectionLength), e.Text));
        //}
    }
}
